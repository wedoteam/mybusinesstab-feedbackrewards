import React from 'react';
import {Modal, StyleSheet, Text, View} from 'react-native';
import {DEVICE_HEIGHT, DEVICE_WIDTH} from "../../styles";

const MBTCommonNoInternetPage = props => {
  const {
    deviceIsOffline,
    ...attributes
  } = props;

  return (
    <Modal
      transparent={false}
      animationType={'none'}
      visible={deviceIsOffline}
      onRequestClose={() => {

      }}>
      <View style={styles.modalBackground}>
        <View style={styles.wrapper}>
          <Text style={styles.textStyle}>No Internet Connection.</Text>
        </View>
      </View>
    </Modal>
  )
};

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  wrapper: {
    backgroundColor: '#FFFFFF',
    height: DEVICE_HEIGHT,
    width: DEVICE_WIDTH,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  textStyle: {
    fontSize: 30,
    color: '#DA453A'
  }
});

export default MBTCommonNoInternetPage;