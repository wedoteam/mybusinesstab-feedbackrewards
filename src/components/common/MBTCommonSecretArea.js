import React from 'react';
import {TouchableOpacity} from 'react-native';

const MBTCommonSecretArea = props => {
  const {
    onActivated
  } = props;

  return (
    <TouchableOpacity delayLongPress={500} onLongPress={onActivated}
                      style={{position: "absolute", top: 0, left: 0, width: 200, height: 200}}/>
  )
};

export default MBTCommonSecretArea;