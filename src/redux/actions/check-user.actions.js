import * as ActionType from './ActionType'
import checkUserService from "../../services/check-user.service";

const enterPhoneAction = (number) => {
  return {
    type: ActionType.ENTER_PHONE,
    payload: number
  }
};

const checkUserRequestAction = () => {
  return {
    type: ActionType.CHECK_USER_REQUEST,
  }
};

const checkUserSuccessAction = (data) => {
  return {
    type: ActionType.CHECK_USER_SUCCESS,
    payload: data,
  };
};

const checkUserErrorAction = (err) => {
  return {
    type: ActionType.CHECK_USER_ERROR,
    payload: err
  }
};

export function checkUserAction(dispatch, userDetails, authValue) {
  dispatch(enterPhoneAction(userDetails.phoneNumber));
  dispatch(checkUserRequestAction());
  return checkUserService(userDetails, authValue)
    .then(response => {
      return new Promise(resolve => {
        switch (response.statusCode) {
          case 100:
            dispatch(checkUserSuccessAction(response.data));
            resolve(true);
            break;

          default:
            dispatch(checkUserSuccessAction({
              name: '',
              emailId: ''
            }));
            resolve(false);
            break;
        }
      })
    })
    .catch((err) => {
      dispatch(checkUserErrorAction(err))
      return Promise.resolve(false)
    })
}