import {refreshSettingsAction} from "./refresh-settings.actions";
import {Actions} from "react-native-router-flux";

export const inactivityResetAction = (dispatch, data) => {
  let promise = [];
  // alert ("HELLO WORLD");
  if (data.fromLoginPage) {
    promise.push(
      new Promise(resolve => {
        resolve(true)
      })
    )
  } else {
    promise.push(
      new Promise(resolve => {
        refreshSettingsAction(dispatch, data.userId).then(() => resolve(true)).catch(() => resolve(false))
      })
    )
  }

  console.log("employee Section As First Page");
  console.log(data.employeeSectionAsFirstPage);
  let page = 'overAllRatingPage'
  if (data.landingPageShow) {
    page = 'landingPage';
  }
  if(data.employeeSectionAsFirstPage){
    page = 'rateEmployeesPage';
  }

  return Promise.all(promise).then(() => {
    Actions.reset(page);
    return Promise.resolve(true)
  })
};