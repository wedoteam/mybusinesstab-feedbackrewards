import * as ActionType from "./ActionType";

export const employeesSelectedAction = (dispatch, data) => {
  dispatch({
    type: ActionType.EMPLOYEES_SELECTED,
    payload: data
  })

  return Promise.resolve(true)
};