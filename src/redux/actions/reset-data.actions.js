import * as ActionType from './ActionType'

export function resetDataAction(dispatch, data) {
  dispatch({
    type: ActionType.RESET_PAGE,
    payload: data
  });
  return Promise.resolve(true)
}
