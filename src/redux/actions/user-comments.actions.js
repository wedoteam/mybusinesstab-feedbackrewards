import * as ActionType from "./ActionType";

export const addUserCommentAction = (dispatch, data) => {
  dispatch({
    type: ActionType.USER_COMMENT,
    payload: data
  })

  return Promise.resolve(true)
};