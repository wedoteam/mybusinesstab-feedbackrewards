import * as ActionType from './ActionType'

export function subscribeAction(dispatch, optData) {
  dispatch({
    type: ActionType.SMS_OPT,
    payload: optData
  });

  return Promise.resolve(optData)
}