import * as ActionType from "./ActionType";

export const servicesSelectedAction = (dispatch, data) => {
  dispatch({
    type: ActionType.SERVICES_SELECTED,
    payload: data
  })

  return Promise.resolve(true)
};