import * as ActionType from "./ActionType";

export const detailRatingAction = (dispatch, data) => {
  dispatch({
    type: ActionType.DETAIL_RATING,
    payload: data
  })

  return Promise.resolve(true)
};