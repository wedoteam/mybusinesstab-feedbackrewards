import * as ActionType from "./ActionType";

export const rateOverAllAction = (dispatch, data) => {
  dispatch({
    type: ActionType.OVERALL_RATING,
    payload: data
  })

  return Promise.resolve(true)
};