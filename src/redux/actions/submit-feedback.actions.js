import * as ActionType from './ActionType'
import userFeedbackService from "../../services/user-feedback.service";

const submitDataRequestAction = () => {
  return {
    type: ActionType.SUBMIT_DATA_REQUEST,
  }
};

const submitDataSuccessAction = (data) => {
  return {
    type: ActionType.SUBMIT_DATA_SUCCESS,
    payload: data,
  };
};

const submitDataErrorAction = (err) => {
  return {
    type: ActionType.SUBMIT_DATA_ERROR,
    payload: err
  }
};

export const setUserPointsAction = (points) => {
  return {
    type: ActionType.SET_POINTS,
    payload: points
  }
};

export function submitFeedbackAction(dispatch, collectedData, authValue) {
  dispatch(submitDataRequestAction());
  return userFeedbackService(collectedData, authValue)
    .then(response => {
      return new Promise(resolve => {
        switch (response.statusCode) {
          case 100:
          case 106:
            if (response.data) {
              dispatch(setUserPointsAction(response.data))
            }
            dispatch(submitDataSuccessAction(response.data));
            resolve(true);
            break;

          default:
            dispatch(submitDataErrorAction({
              name: '',
              emailId: ''
            }));
            resolve(false);
            break;
        }
      })
    })
    .catch((err) => {
      dispatch(submitDataErrorAction(err));
      return Promise.reject(err)
    })
}