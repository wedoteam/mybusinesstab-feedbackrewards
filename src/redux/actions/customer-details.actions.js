import * as ActionType from './ActionType'

export function submitCustomerDetailsAction(dispatch, customerDetails) {
  dispatch({
    type: ActionType.ENTER_DETAILS,
    payload: customerDetails
  });
  return Promise.resolve(true)
}