import * as ActionType from './ActionType'
import refreshSettingsDataService from "../../services/refresh-settings-data.service";

const refreshSettingsSuccessAction = (data) => {
  return {
    type: ActionType.REFRESH_SETTINGS_SUCCESS,
    payload: data,
  };
};

export function refreshSettingsAction(dispatch, userId) {
  return refreshSettingsDataService(userId)
    .then(response => {
      return new Promise(resolve => {
        switch (response.statusCode) {
          case 100:
            dispatch(refreshSettingsSuccessAction(response.data));
            resolve(true);
            break;

          default:
            resolve(false);
            break;
        }
      })
    })
}