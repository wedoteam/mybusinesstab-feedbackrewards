import * as ActionType from './ActionType'
import userLoginService from '../../services/login.service'

const userLoginRequestAction = () => {
  return {
    type: ActionType.USER_LOGIN_REQUEST,
  }
};

const loginSuccessAction = (data) => {
  return {
    type: ActionType.USER_LOGIN_SUCCESS,
    payload: data,
  };
};

const intiDataAction = (data) => {
  return {
    type: ActionType.INITIALIZE,
    payload: data,
  };
};

const loginErrorAction = (err) => {
  return {
    type: ActionType.USER_LOGIN_ERROR,
    payload: err
  }
};

export function loginAction(dispatch, loginDetails) {
  dispatch(userLoginRequestAction());
  return userLoginService(loginDetails)
    .then(response => {
      return new Promise(resolve => {
        switch (response.statusCode) {
          case 100:
            dispatch(loginSuccessAction(response.data));
            // {
            // ...response.data,
            //   id: response.data._id,
            //   timeZone: response.data.settings.preferredTimezone
            // }
            resolve(true);
            break;

          default:
            dispatch(loginErrorAction(response.message));
            resolve(false);
            break;
        }
      })
    })
    .catch((err) => {
      dispatch(loginErrorAction(err));
      return Promise.reject(err)
    })
}