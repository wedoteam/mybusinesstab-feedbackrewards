import * as ActionType from '../actions/ActionType'

const initialState = {
  overAllRatingLevel: 0,
  customerId: '',
  satisfiedCustomer: false,
  overAllRating: undefined,
  rateAttributes: false,
  feedBackPage: undefined,
  notes: '',
  detailRating: {
    attribute1: {
      attribute: '',
      ratingLevel: 0
    },
    attribute2: {
      attribute: '',
      ratingLevel: 0
    },
    attribute3: {
      attribute: '',
      ratingLevel: 0
    }
  },
  interactedEmployees: [],
  servicesUsed: [],
  customerEmailId: "",
  customerDetailsExtraFieldValue: "",
  comments: "",
  customerName: "",
  customerPhoneNumber: "",
  submittedThrough: "direct",
  subscribedOnWeb: false,
  doneButton: false,
  userPoints: 0,
  lastUpdatedPoints: 0,
  targetPoints: 0,
  preferredTimezone: '',
  modalVisible: false
};

export default dataReducer = (state = initialState, action) => {

  switch (action.type) {

    case ActionType.USER_LOGIN_SUCCESS:
      return {
        ...state,
        customerId: action.payload._id,
        preferredTimezone: action.payload.settings.preferredTimezone,
        detailRating: action.payload.detailRating,
        overAllRating: action.payload.overAllRating,
        comment: action.payload.comment,
        rateAttributes: action.payload.rateAttributes,
        feedBackPage: action.payload.feedBackPage,
        employeePageHeader: action.payload.employeePageHeader,
        employeeRatingPageHeader: action.payload.employeeRatingPageHeader,
        servicesRatingPageHeader: action.payload.servicesRatingPageHeader,
        servicesPageHeader: action.payload.servicesPageHeader,
        feedbackRecordedOn: 0,
        createdOn: 0,
      };

    case ActionType.ENTER_PHONE:
      return {
        ...state,
        customerPhoneNumber: action.payload
      };

    case ActionType.CHECK_USER_SUCCESS:      
      return {
        ...state,
        customerName: action.payload.name,
        customerEmailId: action.payload.emailId,
        customerDetailsExtraFieldValue: action.payload.emailId,
        userPoints: action.payload.points,
        notes: action.payload.notes
      };

    case ActionType.CHECK_USER_ERROR:
      return {
        ...state,
        customerName: '',
        customerEmailId: '',
        customerDetailsExtraFieldValue: ''
      };

    case ActionType.ENTER_DETAILS:
      return {
        ...state,
        customerName: action.payload.name,
        customerDetailsExtraFieldValue: action.payload.other
      };

    case ActionType.SMS_OPT:
      return {
        ...state,
        subscribedOnWeb: action.payload
      };

    case ActionType.SET_POINTS:
      return {
        ...state,
        userPoints: action.payload.points, 
        lastUpdatedPoints: action.payload.lastUpdatedPoints,
        targetPoints: action.payload.targetPoints
      };

    case ActionType.REFRESH_SETTINGS_SUCCESS:
      return {
        ...initialState,
        customerId: state.customerId,
        preferredTimezone: state.preferredTimezone,
        detailRating: action.payload.detailRating,
        overAllRating: action.payload.overAllRating,
        comment: action.payload.comment,
        rateAttributes: action.payload.rateAttributes,
        feedBackPage: action.payload.feedBackPage,
        employeePageHeader: action.payload.employeePageHeader,
        employeeRatingPageHeader: action.payload.employeeRatingPageHeader,
        servicesRatingPageHeader: action.payload.servicesRatingPageHeader,
        servicesPageHeader: action.payload.servicesPageHeader,
        feedbackRecordedOn: 0,
        createdOn: 0,
      };

    case ActionType.OVERALL_RATING:
      return {
        ...state,
        overAllRatingLevel: action.payload,
        satisfiedCustomer: parseInt(action.payload) === 5
      }

    case ActionType.DETAIL_RATING:
      return {
        ...state,
        detailRating: action.payload,
        satisfiedCustomer: (state.satisfiedCustomer && parseInt(action.payload.attribute1.ratingLevel) > 4 && parseInt(action.payload.attribute2.ratingLevel) > 4 && parseInt(action.payload.attribute3.ratingLevel) > 4)
      }

    case ActionType.EMPLOYEES_SELECTED:
      
      console.log("EMP SEL");
      console.log(action.payload);
      
      return {
        ...state,
        interactedEmployees: action.payload
      }

    case ActionType.SERVICES_SELECTED:
      return {
        ...state,
        servicesUsed: action.payload
      }

    case ActionType.USER_COMMENT:
      return {
        ...state,
        comments: action.payload,
        doneButton: false
      }

    default:
      return state;

  }
}
