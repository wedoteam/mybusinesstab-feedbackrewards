import * as ActionType from '../actions/ActionType'

const initialState = {
  error: undefined,
  loading: false,
  deviceIsOffline: false
};

export default appReducer = (state = initialState, action) => {

  switch (action.type) {

    case ActionType.USER_LOGIN_REQUEST:
    case ActionType.CHECK_USER_REQUEST:
    case ActionType.SUBMIT_DATA_REQUEST:
      return {
        ...state,
        error: undefined,
        loading: true
      };

    case ActionType.USER_LOGIN_SUCCESS:
    case ActionType.CHECK_USER_SUCCESS:
    case ActionType.SUBMIT_DATA_SUCCESS:
    case ActionType.REFRESH_SETTINGS_SUCCESS:
      return {
        ...state,
        error: undefined,
        loading: false,
      };

    case ActionType.USER_LOGIN_ERROR:
    case ActionType.CHECK_USER_ERROR:
    case ActionType.SUBMIT_DATA_ERROR:
      return {
        ...state,
        loading: false,
        error: typeof action.payload === 'string' ? action.payload : ''
      };

    case ActionType.CONNECTION_STATE_CHANGED:
      return {
        ...state,
        deviceIsOffline: action.payload === 'offline'
      };

    default:
      return state;

  }
}
