import * as ActionType from '../actions/ActionType'
import {MBTNavigation} from "../../helpers/navigation.helper";

const initialState = new MBTNavigation();

export default navReducer = (state = initialState, action) => {

  switch (action.type) {

    case ActionType.USER_LOGIN_SUCCESS:
      return state.updateConfig(action.payload);

    case ActionType.REFRESH_SETTINGS_SUCCESS:
      state.updateConfig(action.payload);
      return state.updateData({
        name: '',
        subscribedOnWeb: false,
        interactedEmployees: [],
        servicesUsed: []
      });

    case ActionType.CHECK_USER_SUCCESS:
    case ActionType.ENTER_DETAILS: 
      return state.updateData({
        name: action.payload.name, 
        emailId: action.payload.emailId
      });

    case ActionType.SMS_OPT:
      return state.updateData({
        subscribedOnWeb: action.payload
      });

    case ActionType.EMPLOYEES_SELECTED:
      return state.updateData({
        interactedEmployees: action.payload,
      });

    case ActionType.SERVICES_SELECTED:
      return state.updateData({
        servicesUsed: action.payload,
      });

    default:
      return state;

  }
}
