import * as ActionType from '../actions/ActionType'

const initialState = {
  user: undefined,
};

export default authReducer = (state = initialState, action) => {

  switch (action.type) {

    case ActionType.USER_LOGIN_REQUEST:
      return {
        ...state,
        user: undefined
      };

    case ActionType.USER_LOGIN_SUCCESS:
      return {
        ...state,
        user: action.payload
      };

    case ActionType.USER_LOGIN_ERROR:
      return {
        ...state,
        user: undefined
      };

    case ActionType.REFRESH_SETTINGS_SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          ...action.payload
        }
      };

    default:
      return state;

  }
}
