import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import devTools from 'remote-redux-devtools';
import {Platform} from "react-native";

import authReducer from './auth.reducer'
import dataReducer from './data.reducer';
import appReducer from "./app.reducer";
import navReducer from "./nav.reducer";

const AppReducers = combineReducers({
  auth: authReducer,
  data: dataReducer,
  app: appReducer,
  nav: navReducer
});

const rootReducer = (state, action) => {
  return AppReducers(state, action);
};

const enhancer = compose(
  applyMiddleware(thunk),
  devTools({
    name: Platform.OS,
    hostname: 'localhost',
    port: 5678
  })
);

let store = createStore(
  rootReducer,
  {},
  enhancer
);

export default store;