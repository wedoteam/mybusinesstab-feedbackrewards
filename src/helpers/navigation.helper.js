import {Actions} from "react-native-router-flux";
import {InactivityIntervalHandler} from "./inactivity-reset.helper";

export class MBTNavigation {

  _config = {
    landingPage: false,
    overAllRatingPage: true,
    rateAttributesPage: true,
    rateEmployeesPage: false,
    rateEmployeesSeparatelyPage: false,
    rateServicesPage: false,
    rateServicesSeparatelyPage: false,
    phoneNumberPage: false,
    customerDetailsPage: false,
    rewardsPage: false,
    commentsPage: false,
    thankYouPage: true,
  };

  _customerData = {
    name: '',
    subscribedOnWeb: false,
    interactedEmployees: [],
    servicesUsed: []
  };
  
  _options = {
    employeeSectionAsFirstPage: false,
    showLandingPage : false, 
    showServicesPage: false
  }
  
  lastConfig: undefined;

  updateData(value: boolean) {
    let updated = false;
    if (value.name !== undefined) {
      this._customerData.name = value.name;
      updated = true;
    }
    if (value.subscribedOnWeb !== undefined) {
      this._customerData.subscribedOnWeb = value.subscribedOnWeb;
      updated = true;
    }
    if (value.interactedEmployees !== undefined) {
      this._customerData.interactedEmployees = value.interactedEmployees;
      updated = true;
    }
    if (value.servicesUsed !== undefined) {
      this._customerData.servicesUsed = value.servicesUsed;
      updated = true;
    }
    if (updated) {
      return this.updateConfig()
    }
    return this;
  }

  updateConfig(config) {
    try {
      if (!config) {
        config = this.lastConfig
      } else {
        this.lastConfig = {
          feedBackPage: config.feedBackPage,
          rateAttributes: config.rateAttributes,
          evaluateEmployeesSeparately: config.evaluateEmployeesSeparately,
          employees: config.employees,
          rateEmployeesSeparately: config.rateEmployeesSeparately,
          evaluateServicesSeparately: config.evaluateServicesSeparately,
          services: config.services,
          rateServicesSeparately: config.rateServicesSeparately,
          addComments: config.addComments,
          employeeSectionAsFirstPage: config.employeeSectionAsFirstPage
        }
      }

      this._config.landingPage = config.feedBackPage && config.feedBackPage.landingPage && config.feedBackPage.landingPage.show;
      this._config.rateAttributesPage = config.rateAttributes;
      this._config.rateEmployeesPage = config.evaluateEmployeesSeparately && (config.employees && config.employees.length > 0);
      this._config.rateEmployeesSeparatelyPage = this._config.rateEmployeesPage && (this._customerData.interactedEmployees.length > 0);
      this._config.rateServicesPage = config.evaluateServicesSeparately && (config.services && config.services.length > 0);
      this._config.rateServicesSeparatelyPage = this._config.rateServicesPage && (this._customerData.servicesUsed.length > 0);
      this._config.phoneNumberPage = config.feedBackPage && config.feedBackPage.phonePage;
      this._config.customerDetailsPage = config.feedBackPage && config.feedBackPage.collectMoreInfo && (config.feedBackPage.showCustomerDetailsPageAlways || (this._customerData.name === ''));
      this._config.rewardsPage = this._config.phoneNumberPage && config.feedBackPage.askRewardsSubscriptionPermission && (this._customerData.subscribedOnWeb === false);
      this._config.commentsPage = config.addComments;

      this._options.employeeSectionAsFirstPage = config.employeeSectionAsFirstPage;
      this._options.showLandingPage = config.feedBackPage.landingPage.show;
      this._options.showServicesPage = config.evaluateServicesSeparately && (config.services && config.services.length > 0);

    } catch (e) {
      console.error(e)
    }

    return this;
  }

  getNextPage() {
    let isCurrentPage = false;
    let nextPage = '';

    if(this._options.employeeSectionAsFirstPage ){
      if((this._customerData.interactedEmployees.length > 0)){
        if(Actions.currentScene == 'rateEmployeesSeparatelyPage'){
          if(this._options.showLandingPage){
            return 'landingPage';
          } else {
            return 'overAllRatingPage';
          }
        }
      } else {
        if(Actions.currentScene == 'rateEmployeesPage'){
          if(this._options.showLandingPage){
            return 'landingPage';
          } else {
            return 'overAllRatingPage';
          }
        }
      }
      if(Actions.currentScene == 'rateAttributesPage'){
        if(this._options.showServicesPage){
          return 'rateServicesPage';
        } else {
          return 'phoneNumberPage';
        }
      }
    }

    Object.keys(this._config).forEach(k => {
      if (!isCurrentPage && k === Actions.currentScene) {
        isCurrentPage = true
      } else if (nextPage === '' && isCurrentPage && this._config[k]) {
        nextPage = k;
      }
    });

    console.log({"currentPage": Actions.currentScene, "nextPage": nextPage});

    return nextPage;
  }

  getPreviousPage() {
    let isCurrentPage = false;
    let nextPage = '';

    Object.keys(this._config).reverse().forEach(k => {
      if (!isCurrentPage && k === Actions.currentScene) {
        isCurrentPage = true
      } else if (nextPage === '' && isCurrentPage && this._config[k]) {
        nextPage = k;
      }
    });

    return nextPage;
  }

  navigateToNextPage() {
    InactivityIntervalHandler.clearInterval();
    Actions.reset(this.getNextPage())
  }

  navigateToPreviousPage() {
    Actions.reset(this.getPreviousPage())
  }
}