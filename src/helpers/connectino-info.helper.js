import {NetInfo} from "react-native";
import store from "../redux/reducers";
import * as ActionType from '../redux/actions/ActionType'

function handleConnectivityInfo(connectionInfo) {
  console.log(connectionInfo)
  if (connectionInfo.type === 'none') {
    store.dispatch({
      type: ActionType.CONNECTION_STATE_CHANGED,
      payload: 'offline'
    })
  } else {
    store.dispatch({
      type: ActionType.CONNECTION_STATE_CHANGED,
      payload: 'online'
    })
  }
}

export class ConnectionInfo {
  static startWatch() {
    NetInfo.addEventListener(
      'connectionChange',
      handleConnectivityInfo
    );
  }

  static stopWatch() {
    NetInfo.removeEventListener(
      'connectionChange',
      handleConnectivityInfo
    );
  }
}