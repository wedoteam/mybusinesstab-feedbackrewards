export class InactivityIntervalHandler {
  static intervalInt;
  static lastActiveAt;
  static data;
  static callback;

  static clearInterval(tries = 0) {
    console.log("STOPPING INTERVAL", new Date().getTime());
    return new Promise((resolve, reject) => {
      try {
        if (InactivityIntervalHandler.intervalInt !== undefined) {
          clearInterval(InactivityIntervalHandler.intervalInt);
          InactivityIntervalHandler.intervalInt = undefined
        }
        resolve(true)
      } catch (e) {
        if (tries < 3) {
          tries = tries + 1;
          InactivityIntervalHandler.clearInterval(tries)
        } else {
          reject(e)
        }
      }
    })
  }

  static userActivity() {
    InactivityIntervalHandler.lastActiveAt = new Date().getTime()
  }

  static start() {
    InactivityIntervalHandler.clearInterval().then(() => {
      InactivityIntervalHandler.lastActiveAt = new Date().getTime();

      InactivityIntervalHandler.intervalInt = setInterval(() => {

        if (InactivityIntervalHandler.intervalInt !== undefined && InactivityIntervalHandler.data !== undefined) {
          let time = new Date().getTime() - InactivityIntervalHandler.lastActiveAt;

          console.log("inside interval, CDETAILS", parseInt(time / 1000), InactivityIntervalHandler.data.user.settings.inactivityResetTimer, new Date().getTime());

          if (parseInt(time / 1000) >= InactivityIntervalHandler.data.user.settings.inactivityResetTimer - 1) {
            InactivityIntervalHandler.clearInterval().then(() => {
              return InactivityIntervalHandler.data.inactivityReset({
                userId: InactivityIntervalHandler.data.user._id,
                landingPageShow: InactivityIntervalHandler.data.user.feedBackPage.landingPage.show,
                employeeSectionAsFirstPage : InactivityIntervalHandler.data.user.employeeSectionAsFirstPage
              })
            }).then(() => {
              if (InactivityIntervalHandler.callback !== undefined && typeof InactivityIntervalHandler.callback === "function") {
                InactivityIntervalHandler.callback()
              }
            })
          }
        }
      }, 2000)
    })
  }
}

export function formattedNumber(number) {
  let numbers = number.toString().replace(/\D/g, ''),
    char = {0: '(', 3: ') ', 6: ' - '};
  let numberToReturn = '';
  for (let i = 0; i < numbers.length; i++) {
    numberToReturn += (char[i] || '') + numbers[i];
  }
  return numberToReturn;
}