import React, {Component} from 'react';
import {ScrollView, StatusBar, StyleSheet, TouchableOpacity, View, WebView, Modal, Linking, Text} from 'react-native';
import {connect} from "react-redux";
import {InactivityIntervalHandler} from "../helpers/inactivity-reset.helper";
import {inactivityResetAction} from "../redux/actions/inactivity-reset.actions";
import {DEVICE_HEIGHT, DEVICE_WIDTH} from "../styles";
import MBTCommonNoInternetPage from "../components/common/MBTCommonNoInternetPage";
import MBTCommonLoader from "../components/common/MBTCommonLoader";
import KeepAwake from "react-native-keep-awake";
import MBTCommonSecretArea from "../components/common/MBTCommonSecretArea";

import RNRestart from 'react-native-restart';
import {setJSExceptionHandler} from 'react-native-exception-handler';

import VersionCheck from 'react-native-version-check';

import TimerMixin from 'react-timer-mixin';
mixins: [TimerMixin];

const errorHandler = (e, isFatal) => {
  if (isFatal) {
    RNRestart.Restart();
  } else {
    console.log(e);
  }
};
setJSExceptionHandler(errorHandler); 

class LandingPage extends Component {

  state = {
    loading: false, 
    modalVisible: false,
  }

  constructor() {
    super();
    this._onClick = this._onClick.bind(this)
  }

  componentWillMount() {
    KeepAwake.activate();    
    var currv = VersionCheck.getCurrentVersion();
    var storeURL = VersionCheck.getStoreUrl();
    VersionCheck.getLatestVersion()
    .then(async latestVersion => {
      var curVArray = currv.split(".");
      var latVArray = latestVersion.split(".");
      
      if(latVArray[1] > curVArray[1]){
        // alert('NO 2 UPDATE');
        this.setState({modalVisible: true}); 
      }
      if(latVArray[0] > curVArray[0]){
        // alert('NO 1 UPDATE');
        this.setState({modalVisible: true});
      }
    });
  }

  // componentWillUnmount() {
  //   KeepAwake.deactivate()
  // }

  componentDidMount() {

    this.interval = setInterval(() => { 
      RNRestart.Restart();
    }, 28800000); // 8 Hours

    // alert('LANDING PAGE');
    this._initializeInactivityChecker();
  }

  _initializeInactivityChecker() {
    InactivityIntervalHandler.callback = this._initializeInactivityChecker.bind(this);
    InactivityIntervalHandler.data = this.props;
    InactivityIntervalHandler.start()
  }

  _onClick() {
    this.setState({
      loading: true
    })
    setTimeout(() => {
      this.setState({
        loading: false
      })
      this.props.nav.navigateToNextPage()
    }, 350)
  }

  render() {
    return (
      <View style={appStyles.container}>

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            
          }}
          >
          <View style={{flex: 1, alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.7)', flexDirection: 'row', justifyContent: 'center'}}>
            <View style={{flexDirection: 'column', alignItems: 'center', justifyContent:'center', alignItems:'center', width: 600, height: 200, padding: 20, backgroundColor: '#ffffff' }}>
              <View style={{flex: 1}}>
                <Text style={{fontSize: 24, textAlign: 'center'}}>There is a Major release for the App that you must Update in order to continue using.</Text>
              </View>

              <View style={{flex: 1}}>
              <TouchableOpacity
                style={appStyles.submitButton}
                onPress={ async () => {
                  Linking.openURL(await VersionCheck.getStoreUrl());
                }}
                activeOpacity={0.65}>
                <Text style={appStyles.submitButtonText}>UPDATE NOW</Text>
              </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>


        <MBTCommonSecretArea onActivated={() => {
          alert('secret area activated')
        }}/>
        <MBTCommonLoader loading={this.props.loading === true || this.state.loading === true}/>
        <MBTCommonNoInternetPage deviceIsOffline={this.props.deviceIsOffline}/>
        <TouchableOpacity onPress={this._onClick} activeOpacity={1}>
          <WebView
            onLoadEnd={this.webViewLoadingEnd}
            onLoadStart={this.webViewLoadingStart}
            scalesPageToFit={true}
            scrollEnabled={false}
            source={{html: this.props.user.feedBackPage.landingPage.text}}
            style={{
              width: DEVICE_WIDTH-100,
              height: DEVICE_HEIGHT,
              alignItems: 'center', 
              justifyContent: 'center'
            }}/>
        </TouchableOpacity>
      </View>
    );
  }
}

const appStyles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }, 
  submitButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DA453A',
    width: 150,
    borderRadius: 30,
    padding: 8,
    zIndex: 100,
  },
});

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    data: state.data,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline,
    nav: state.nav
  }
}

function mapDispatchToProps(dispatch) {
  return {
    // inactivityReset: (data) => inactivityResetAction(dispatch, data)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LandingPage)
