import React, {Component} from 'react';
import {Alert, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View, WebView, Dimensions, Platform} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {connect} from "react-redux";
import {checkUserAction} from "../redux/actions/check-user.actions";
import MBTCommonLoader from "../components/common/MBTCommonLoader";
import {formattedNumber, InactivityIntervalHandler} from "../helpers/inactivity-reset.helper";
import {inactivityResetAction} from "../redux/actions/inactivity-reset.actions";
import {DEVICE_HEIGHT, DEVICE_WIDTH} from "../styles";
import MBTCommonNoInternetPage from "../components/common/MBTCommonNoInternetPage";
import KeepAwake from "react-native-keep-awake";

import Toast from 'react-native-simple-toast';

const removetexticon = require('../assets/cut_text.png');

class NumberPadButton extends Component {
  render() {

    var buttonClass = appStyles.numberPadButtonNext;
    if(this.props.phoneNumber.length == 10){
      buttonClass = appStyles.numberPadButtonNextActive;
    } 

    return (
      <TouchableOpacity
        activeOpacity={0.3}
        onPress={this.props.onPress}
        style={this.props.next ? buttonClass : appStyles.numberPadButton}
      >
        {!this.props.next &&
        <Text style={appStyles.numberPadButtonText}>{this.props.children}</Text>
        }
        {this.props.next &&
        <View>
        <Text style={appStyles.numberPadButtonText} >Next <Icon name={'chevron-right'} size={24} color='#fff'/></Text>
        </View>
        }
      </TouchableOpacity>
    )
  }
}

class PhoneNumberPage extends Component {

  state = {
    number: "",
    loading: false
  };

  constructor() {
    super();
    this.dialPadNumberPressed = this.dialPadNumberPressed.bind(this);
    this.deleteNumber = this.deleteNumber.bind(this);
    this.renderNumberPad = this.renderNumberPad.bind(this);
  }

  componentWillMount() {
    console.log("\n\nKEEPING AWAKE\n======================\n\n")
    KeepAwake.activate()
  }

  // componentWillUnmount() {
  //   KeepAwake.deactivate()
  // }

  componentDidMount() {
    InactivityIntervalHandler.callback = undefined;
    InactivityIntervalHandler.data = this.props;
    InactivityIntervalHandler.start()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error !== undefined) {
      setTimeout(() => {
        Toast.showWithGravity(
          `${nextProps.error}`,
          Toast.LONG,
          Toast.CENTER
        );
      }, 100)
    }
  }

  dialPadNumberPressed(number) {
    InactivityIntervalHandler.userActivity();
    if (number === 'next') {
      if (this.state.number.length === 0) {
        if (!this.props.user.feedBackPage.makePhoneNumberMandatory) {
          Alert.alert(
            'Are you sure?',
            'You might loose Rewards.',
            [
              {
                text: 'YES, I PASS', onPress: () => {
                  this.setState({
                    loading: true
                  })
                  setTimeout(() => {
                    this.setState({
                      loading: false
                    })
                    this.props.nav.navigateToNextPage()
                  }, 350)
                }
              },
              {text: 'ENTER PHONE', style: 'cancel'}
            ],
            {cancelable: false}
          );

          return
        } else {
          Alert.alert(
            'Phone Number is required',
            'Please enter your phone number',
            [
              {text: 'ENTER PHONE', style: 'cancel'}
            ],
            {cancelable: false}
          );
          return;
        }
      } else if (this.state.number.length > 0 && this.state.number.length < 10) {
        Toast.showWithGravity(
          `Wrong Phone Number Format`,
          Toast.LONG,
          Toast.CENTER
        );
        return;
      }

      this.setState({
        loading: true
      })

      this.props.checkUser({
        phoneNumber: this.state.number,
        businessOwnerUserName: this.props.user.userName
      }, this.props.user.sessionId).then(() => {
        setTimeout(() => {
          this.setState({
            loading: false
          })
          this.props.nav.navigateToNextPage()
        }, 350)
      }).catch(err => {
        this.setState({
          loading: false
        })
      })
    } else {
      let oldNumber = this.state.number;
      if (oldNumber.length < 10) {
        let newNumber = oldNumber.toString() + number;
        this.setState({
          number: newNumber
        })
      }
    }
  }

  deleteNumber() {
    InactivityIntervalHandler.userActivity();
    let oldNumber = this.state.number;
    if (oldNumber.length > 0) {
      oldNumber = oldNumber.toString().split('');
      oldNumber.pop();
      let newNumber = oldNumber.join('');
      this.setState({
        number: newNumber
      })
    }
  }

  renderNumberPad(number, next?: boolean) {
    return (
      <NumberPadButton key={number}
                       phoneNumber={this.state.number}
                       onPress={() => {
                         this.dialPadNumberPressed(number)
                       }}
                       next={next}>
        {number}
      </NumberPadButton>
    )
  }

  render() {
    return (
      <View style={appStyles.container} keyboardDismissMode={'on-drag'}>
        <View style={appStyles.container}>
          <MBTCommonLoader loading={this.props.loading === true || this.state.loading === true}/>
          <MBTCommonNoInternetPage deviceIsOffline={this.props.deviceIsOffline}/>
          <View style={appStyles.leftWebView}>
            <WebView
              onLoadEnd={this.webViewLoadingEnd}
              onLoadStart={this.webViewLoadingStart}
              scalesPageToFit={Platform.os == 'ios'? false : true }
              scrollEnabled={false}
              source={{html: this.props.user.feedBackPage.phoneNumberPage.text ? this.props.user.feedBackPage.phoneNumberPage.text : ''}}
              style={{
                paddingTop: 30,
                width: DEVICE_WIDTH / 2,
                height: DEVICE_HEIGHT * 0.6,
                backgroundColor: '#FFFFFF'
              }}/>
          </View>
          <View style={appStyles.numberPadContainer}>
            <View style={appStyles.mobileNumberWrapper}>
              <Text
                style={appStyles.mobileNumber}>
                {this.state.number.length > 0 ? formattedNumber(this.state.number) : 'ENTER PHONE #'}
              </Text>
              {this.state.number.length > 0 &&
              <TouchableOpacity style={{position: 'absolute', right: 0, padding: 20}} activeOpacity={0.75}
                                onPress={this.deleteNumber}>
                <Image source={removetexticon}/>
              </TouchableOpacity>
              }
            </View>
            <View style={appStyles.numberRow}>
              {[1, 2, 3].map(n => this.renderNumberPad(n))}
            </View>
            <View style={appStyles.numberRow}>
              {[4, 5, 6].map(n => this.renderNumberPad(n))}
            </View>
            <View style={appStyles.numberRow}>
              {[7, 8, 9].map(n => this.renderNumberPad(n))}
            </View>
            <View style={appStyles.numberRow}>
              {[0].map(n => this.renderNumberPad(n))}
              {['next'].map(n => this.renderNumberPad(n, true))}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

var {height, width} = Dimensions.get('window');

const appStyles = StyleSheet.create({
  container: {
    backgroundColor: '#e46570',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  leftWebView: {
    flex: 1,
    width: DEVICE_WIDTH,
    height: DEVICE_HEIGHT,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    paddingTop: 70,
    paddingBottom: 70
  },
  numberPadContainer: {
    flex: 1,
    width: DEVICE_WIDTH,
    height: DEVICE_HEIGHT,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap'
  },
  numberPadButton: {
    borderWidth: 4,
    borderColor: '#F0B1B7',
    alignItems: 'center',
    justifyContent: 'center',
    width: (width * 8)/100,
    height: (width * 8)/100,
    margin: 10,
    backgroundColor: 'transparent',
    borderRadius: 500,
  },
  numberPadButtonNextActive:{
    borderWidth: 4,
    borderColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
    width: (width * 18)/100,
    height: (width * 8)/100,
    margin: 10,
    backgroundColor: 'transparent',
    borderRadius: 500
  }, 
  numberPadButtonNext: {
    borderWidth: 4,
    borderColor: '#F0B1B7',
    alignItems: 'center',
    justifyContent: 'center',
    width: (width * 18)/100,
    height: (width * 8)/100,
    margin: 10,
    backgroundColor: 'transparent',
    borderRadius: 500
  },
  numberPadButtonText: {
    fontSize: 30,
    color: '#fff'
  },
  mobileNumberWrapper: {
    flexDirection: 'row',
    borderRadius: 40,
    paddingHorizontal: 20,
    paddingVertical: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    width: 340,
    marginBottom: 30
  },
  mobileNumber: {
    fontSize: 24,
    color: '#FFF'
  },
  numberRow: {
    flexDirection: 'row'
  }
});

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    data: state.data,
    loading: state.app.loading,
    error: state.app.error,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline,
    nav: state.nav
  }
}

function mapDispatchToProps(dispatch) {
  return {
    checkUser: (userDetails, authValue) => checkUserAction(dispatch, userDetails, authValue),
    inactivityReset: (data) => inactivityResetAction(dispatch, data)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhoneNumberPage)
