import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import {formattedNumber, InactivityIntervalHandler} from "../helpers/inactivity-reset.helper";
import {subscribeAction} from "../redux/actions/subscribe.actions";
import {inactivityResetAction} from "../redux/actions/inactivity-reset.actions";
import {DEVICE_WIDTH} from "../styles";
import MBTCommonNoInternetPage from "../components/common/MBTCommonNoInternetPage";
import MBTCommonLoader from "../components/common/MBTCommonLoader";
import KeepAwake from "react-native-keep-awake";

class RewardsPage extends Component {

  state = {
    loading: false
  }

  componentDidMount() {
    InactivityIntervalHandler.callback = undefined;
    InactivityIntervalHandler.data = this.props;
    InactivityIntervalHandler.start()
  }

  componentWillMount() {
    KeepAwake.activate()
  }

  // componentWillUnmount() {
  //   KeepAwake.deactivate()
  // }

  _onPress(data) {
    this.setState({
      loading: true
    })
    InactivityIntervalHandler.clearInterval();
    this.props.subscribe(data).then(() => {
      setTimeout(() => {
        this.setState({
          loading: false
        })
        this.props.nav.navigateToNextPage()
      }, 350)
    })
  }

  render() {
    return (
      <ScrollView contentContainerStyle={appStyles.container} keyboardDismissMode={'on-drag'}>
        <View style={appStyles.container}>
          <MBTCommonLoader loading={this.props.loading === true || this.state.loading === true}/>
          <MBTCommonNoInternetPage deviceIsOffline={this.props.deviceIsOffline}/>
          <Text style={appStyles.textLarge}>Want to Participate in</Text>
          <View style={appStyles.line2}>
            <Text style={appStyles.textLargeRedBold}>Our EASY Rewards </Text>
            <Text style={appStyles.textLarge}>Program?</Text>
          </View>
          <Text style={appStyles.textMedium}>
            By participating you Consent to Receive Text Messages
          </Text>
          <View style={appStyles.line4}>
            <Text style={appStyles.textMedium}>from</Text>
            <Text style={appStyles.textMediumRed}> MyBusinessTab. </Text>
            <Text style={appStyles.textMedium}>You can always stop by texting Stop.</Text>
          </View>
          <Text style={appStyles.textLargeRed}>{formattedNumber(this.props.data.customerPhoneNumber)}</Text>
          <View style={appStyles.buttons}>
            <TouchableOpacity
              style={appStyles.noButton}
              onPress={this._onPress.bind(this, false)}
              activeOpacity={0.65}>
              <Text style={appStyles.noButtonText}>NO, THANKS</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={appStyles.yesButton}
              onPress={this._onPress.bind(this, true)}
              activeOpacity={0.65}>
              <Text style={appStyles.yesButtonText}>YES, PLEASE!</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const appStyles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  displayBox: {
    width: DEVICE_WIDTH * 0.8,
    flex: 1
  },
  line1: {},
  line2: {
    flexDirection: 'row'
  },
  line4: {
    flexDirection: 'row'
  },
  buttons: {
    flexDirection: 'row'
  },
  textLarge: {
    fontSize: 45,
  },
  textLargeRed: {
    fontSize: 45,
    color: '#DA453A',
    paddingTop: 30,
    paddingBottom: 30
  },
  textLargeRedBold: {
    fontSize: 45,
    color: '#DA453A',
    fontWeight: '900'
  },
  textMedium: {
    fontSize: 25,
  },
  textMediumRed: {
    fontSize: 25,
    color: '#DA453A',
  },
  noButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderColor: '#DA453A',
    borderWidth: 1,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 30,
    paddingRight: 30,
    zIndex: 100,
    marginRight: 20
  },
  yesButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DA453A',
    borderRadius: 30,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 30,
    paddingRight: 30,
    zIndex: 100,
  },
  noButtonText: {
    fontSize: 14,
    fontWeight: '100',
    color: '#DA453A'
  },
  yesButtonText: {
    fontSize: 14,
    fontWeight: '100',
    color: '#fff'
  },
});

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    data: state.data,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline,
    nav: state.nav
  }
}

function mapDispatchToProps(dispatch) {
  return {
    subscribe: (optData) => subscribeAction(dispatch, optData),
    inactivityReset: (data) => inactivityResetAction(dispatch, data)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RewardsPage)