import React, {Component} from 'react';
import {PixelRatio, ScrollView, StyleSheet, Text, View, TouchableOpacity, Modal, Linking, Dimensions} from 'react-native';
import {connect} from "react-redux";
import MBTCommonNoInternetPage from "../components/common/MBTCommonNoInternetPage";
import StarRating from "react-native-star-rating";
import {InactivityIntervalHandler} from "../helpers/inactivity-reset.helper";
import {inactivityResetAction} from "../redux/actions/inactivity-reset.actions";
import {rateOverAllAction} from "../redux/actions/overall-rating.actions";
import MBTCommonLoader from "../components/common/MBTCommonLoader";
import KeepAwake from "react-native-keep-awake";
import MBTCommonSecretArea from "../components/common/MBTCommonSecretArea";

import VersionCheck from 'react-native-version-check';

var {height, width} = Dimensions.get('window');

class OverAllRatingPage extends Component {

  state = {
    overAllRatingLevel: 0,
    loading: false, 
    modalVisible: false,
  };

  constructor() {
    super();
    this.onStarRatingPress = this.onStarRatingPress.bind(this);
    console.log("PixelRatio.get()", PixelRatio.get())
  }

  componentWillMount() {
    KeepAwake.activate();
    this._initializeInactivityChecker(); 

    var currv = VersionCheck.getCurrentVersion();
    var storeURL = VersionCheck.getStoreUrl();
    VersionCheck.getLatestVersion()
    .then(async latestVersion => {
      var curVArray = currv.split(".");
      var latVArray = latestVersion.split(".");
      
      if(latVArray[1] > curVArray[1]){
        // alert('NO 2 UPDATE');
        this.setState({modalVisible: true}); 
      }
      if(latVArray[0] > curVArray[0]){
        // alert('NO 1 UPDATE');
        this.setState({modalVisible: true});
      }
    });
  }

  _initializeInactivityChecker() {
    InactivityIntervalHandler.callback = this.props.nav._config.landingPage === true ? undefined : this._initializeInactivityChecker.bind(this);
    InactivityIntervalHandler.data = this.props;
    InactivityIntervalHandler.start()
  }

  onStarRatingPress(rating) {
    this.setState({
      overAllRatingLevel: rating,
      loading: true
    });

    this.props.rateOverAll(rating).then(() => {
      setTimeout(() => {
        this.setState({
          loading: false
        })
        this.props.nav.navigateToNextPage()
      }, 350)
    })
  }

  render() {
    let reward, rewards;

    if (this.props.user.overAllRating.showRewards) {
      rewards = this.props.user.rewards.availableOffers.filter(r => (!r.deleted && r.selected));
      if (rewards.length > 0) {
        reward = rewards[0];
        console.log(reward);
      }
    }

    return (
      <View style={appStyles.container}>

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            
          }}
          >
          <View style={{flex: 1, alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.7)', flexDirection: 'row', justifyContent: 'center'}}>
            <View style={{flexDirection: 'column', alignItems: 'center', justifyContent:'center', alignItems:'center', width: 600, height: 200, padding: 20, backgroundColor: '#ffffff' }}>
              <View style={{flex: 1}}>
                <Text style={{fontSize: 24, textAlign: 'center'}}>There is a Major release for the App that you must Update in order to continue using.</Text>
              </View>

              <View style={{flex: 1}}>
              <TouchableOpacity
                style={appStyles.submitButton}
                onPress={ async () => {
                  Linking.openURL(await VersionCheck.getStoreUrl());
                }}
                activeOpacity={0.65}>
                <Text style={appStyles.submitButtonText}>UPDATE NOW</Text>
              </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        <MBTCommonSecretArea onActivated={() => {
          alert('secret area activated')
        }}/>
        <MBTCommonLoader loading={this.props.loading === true || this.state.loading === true}/>
        <MBTCommonNoInternetPage deviceIsOffline={false}/>
        <View style={appStyles.container}>
          <Text style={appStyles.title}>{this.props.user.overAllRating.overAllRatingTitle.toUpperCase()}</Text>
          <View style={appStyles.titleUnderline}/>
          <StarRating
            disabled={false}
            maxStars={5}
            rating={this.state.overAllRatingLevel}
            selectedStar={(rating) => this.onStarRatingPress(rating)}
            starSize={(width / 10)}
            emptyStar={require('../assets/star.png')}
            fullStar={require('../assets/selected_star.png')}
            buttonStyle={appStyles.starButtonStyle}
          />
          {(reward && this.props.user.overAllRating.showRewards) &&
          <View style={{alignItems: 'center', justifyContent: 'center',padding: 40}}>
            <Text style={appStyles.earnRewardsText}>EARN REWARDS</Text>
            <Text style={appStyles.rewardText}>{reward.title}</Text>
          </View>
          }
        </View>
      </View>
    );
  }
}

const appStyles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 40,
    fontWeight: '900',
    color: '#303646',
  },
  titleUnderline: {
    backgroundColor: '#303646',
    width: 150,
    height: 1
  },
  starButtonStyle: {
    margin: 40
  },
  earnRewardsText: {
    fontSize: 30,
    fontWeight: '500',
    color: '#303646',
  },
  rewardText: {
    fontSize: 30,
    fontWeight: '500',
    color: '#FA1C4A',
    textAlign: 'center'
  }, 
  submitButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DA453A',
    width: 150,
    borderRadius: 30,
    padding: 8,
    zIndex: 100,
  },
});

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    data: state.data,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline,
    nav: state.nav
  }
}

function mapDispatchToProps(dispatch) {
  return {
    inactivityReset: (data) => {
      if(data.landingPageShow){
        inactivityResetAction(dispatch, data);
      } else {
        // alert("DISABLED API");
      }
    }, 
    rateOverAll: (data) => rateOverAllAction(dispatch, data),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OverAllRatingPage);