import React, {Component} from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import {connect} from "react-redux";
import MBTCommonNoInternetPage from "../components/common/MBTCommonNoInternetPage";
import StarRating from "react-native-star-rating";
import {InactivityIntervalHandler} from "../helpers/inactivity-reset.helper";
import {inactivityResetAction} from "../redux/actions/inactivity-reset.actions";
import {Config} from "../config";
import {employeesSelectedAction} from "../redux/actions/employees-selected.actions";
import {DEVICE_WIDTH} from "../styles";
import MBTCommonLoader from "../components/common/MBTCommonLoader";
import KeepAwake from "react-native-keep-awake";

class RateEmployeesSeparatelyPage extends Component {

  state = {
    overAllRatingLevel: 0,
    loading: false
  };

  constructor() {
    super();
    this.onStarRatingPress = this.onStarRatingPress.bind(this)
  }

  componentWillMount() {
    KeepAwake.activate()
  }

  // componentWillUnmount() {
  //   KeepAwake.deactivate()
  // }

  componentDidMount() {
    this._initializeInactivityChecker();
  }

  _initializeInactivityChecker() {
    InactivityIntervalHandler.callback = this._initializeInactivityChecker.bind(this);
    InactivityIntervalHandler.data = this.props;
    InactivityIntervalHandler.start()
  }

  onStarRatingPress(emp, rating) {
    emp.ratingLevel = rating;
    emp.employeeId = emp._id;
    this.forceUpdate(); 

    if (this.props.data.interactedEmployees.filter(e => !e.ratingLevel).length === 0) {
      this.setState({
        loading: true
      })
      this.props.employeesSelected(this.props.data.interactedEmployees).then(() => {
        setTimeout(() => {
          this.setState({
            loading: false
          })
          this.props.nav.navigateToNextPage()
        }, 350)
      })
    }
  }

  render() {
    return (
      <View style={appStyles.container} keyboardDismissMode={'on-drag'}>
        <View style={appStyles.container}>
          <MBTCommonLoader loading={this.props.loading === true || this.state.loading === true}/>
          <MBTCommonNoInternetPage deviceIsOffline={false}/>
          <View style={appStyles.container}>
            <View style={{width: DEVICE_WIDTH, alignItems: 'center', justifyContent: 'center', marginTop: 20}}>
              <Text style={appStyles.title}>{this.props.user.employeeRatingPageHeader.toUpperCase()}</Text>
            </View>
            <ScrollView>
            {this.props.data.interactedEmployees.map(emp => {
              return (
                <View key={emp._id}
                      style={{marginTop: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                  <View style={appStyles.employeeItem}>
                    <Image style={{borderRadius: 4}} source={{ 
                      uri: (emp.image === undefined ? Config.PLACEHOLDER_EMP_IMAGE : (Config.IMAGE_URL_PREFIX + emp.image)),
                      width: 120,
                      height: 120
                    }}/>
                    <Text>
                      {emp.name}
                    </Text>
                  </View>
                  <StarRating
                    disabled={false}
                    maxStars={5}
                    rating={emp.ratingLevel}
                    selectedStar={(rating) => this.onStarRatingPress(emp, rating)}
                    starSize={60}
                    emptyStar={require('../assets/star.png')}
                    fullStar={require('../assets/selected_star.png')}
                    buttonStyle={appStyles.starButtonStyle}
                  />
                </View>
              )
            })}
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}

const appStyles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1
  },
  starButtonStyle: {
    margin: 20
  },
  employeeItem: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: 110,
    width: 150,
    margin: 15
  },
  title: {
    fontSize: 25,
    fontWeight: '700',
    color: '#303646',
    marginTop: 50
  }
});

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    data: state.data,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline,
    nav: state.nav
  }
}

function mapDispatchToProps(dispatch) {
  return {
    inactivityReset: (data) => inactivityResetAction(dispatch, data),
    employeesSelected: (data) => employeesSelectedAction(dispatch, data)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RateEmployeesSeparatelyPage);