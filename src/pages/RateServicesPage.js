import React, {Component} from 'react';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {connect} from "react-redux";
import MBTCommonNoInternetPage from "../components/common/MBTCommonNoInternetPage";
import {Config} from "../config";
import {inactivityResetAction} from "../redux/actions/inactivity-reset.actions";
import {servicesSelectedAction} from "../redux/actions/services-selected.actions";
import MBTCommonLoader from "../components/common/MBTCommonLoader";
import KeepAwake from "react-native-keep-awake";
import chunkArray from "../helpers/array-chunk.helper";

class RateServicesPage extends Component {

  state = {
    overAllRatingLevel: 0,
    selectedServices: [],
    loading: false
  };

  constructor() {
    super();
    this.getStyle = this.getStyle.bind(this);
    this._onNext = this._onNext.bind(this);
  }

  componentWillMount() {
    KeepAwake.activate()
  }

  // componentWillUnmount() {
  //   KeepAwake.deactivate()
  // }

  // componentDidMount() {
  //   this._initializeInactivityChecker();
  // }
  //
  // _initializeInactivityChecker() {
  //   InactivityIntervalHandler.callback = this._initializeInactivityChecker.bind(this);
  //   InactivityIntervalHandler.data = this.props;
  //   InactivityIntervalHandler.start()
  // }

  selectService(service) {
    let selectedServices = this.state.selectedServices;
    let currentIndex = selectedServices.indexOf(service);
    if (currentIndex >= 0) {
      selectedServices.splice(currentIndex, 1)
    } else {
      selectedServices.push(service)
    }

    this.setState({
      selectedServices
    })
  }

  _onNext() {
    this.setState({
      loading: true
    })
    this.props.servicesSelected(this.state.selectedServices).then(() => {
      setTimeout(() => {
        this.setState({
          loading: false
        })
        this.props.nav.navigateToNextPage()
      }, 350)
    })
  }

  getStyle(serv) {
    if (this.state.selectedServices.indexOf(serv) >= 0) {
      return appStyles.selectedService
    }
    return appStyles.unSelectedService
  }

  render() {
    return (
      <ScrollView contentContainerStyle={appStyles.container} keyboardDismissMode={'on-drag'}>
        <View style={appStyles.container}>
          <MBTCommonLoader loading={this.props.loading === true || this.state.loading === true}/>
          <MBTCommonNoInternetPage deviceIsOffline={false}/>
          <View style={appStyles.container}>
            <Text style={appStyles.title}>{this.props.user.servicesPageHeader.toUpperCase()}</Text>
            {chunkArray(this.props.user.services, 4).map((arr, index) => {
              return (
                <View style={appStyles.servicesRow} key={index}>
                  {arr.map(serv => {
                    console.log(Config.IMAGE_URL_PREFIX + serv.image);
                    return (
                      <TouchableOpacity key={serv._id} onPress={this.selectService.bind(this, serv)}
                                        style={appStyles.serviceItem} activeOpacity={0.9}>
                        <Image style={this.getStyle(serv)} source={{
                          uri: (serv.image === undefined ? Config.PLACEHOLDER_EMP_IMAGE : (Config.IMAGE_URL_PREFIX + serv.image)),
                          width: 100,
                          height: 100
                        }}/>
                        <Text>
                          {serv.name}
                        </Text>
                      </TouchableOpacity>
                    )
                  })}
                </View>
              )
            })}

            <View style={{marginTop: 30}}>
              <TouchableOpacity
                style={appStyles.submitButton}
                onPress={this._onNext}
                activeOpacity={0.65}>
                <Text style={appStyles.submitButtonText}>NEXT</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const appStyles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  submitButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DA453A',
    width: 150,
    borderRadius: 30,
    padding: 8,
    zIndex: 100,
  },
  submitButtonText: {
    fontSize: 18,
    fontWeight: '100',
    color: '#fff'
  },
  servicesRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  serviceItem: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: 150,
    width: 150,
    margin: 25
  },
  selectedService: {
    borderColor: '#FA1C4A',
    borderWidth: 3,
  },
  unSelectedService: {},
  title: {
    fontSize: 25,
    fontWeight: '700',
    color: '#303646',
  }
});

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    data: state.data,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline,
    nav: state.nav
  }
}

function mapDispatchToProps(dispatch) {
  return {
    inactivityReset: (data) => inactivityResetAction(dispatch, data),
    servicesSelected: (data) => servicesSelectedAction(dispatch, data)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RateServicesPage);