import React, {Component} from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import {connect} from "react-redux";
import MBTCommonNoInternetPage from "../components/common/MBTCommonNoInternetPage";
import StarRating from "react-native-star-rating";
import {InactivityIntervalHandler} from "../helpers/inactivity-reset.helper";
import {inactivityResetAction} from "../redux/actions/inactivity-reset.actions";
import {Config} from "../config";
import {servicesSelectedAction} from "../redux/actions/services-selected.actions";
import {DEVICE_WIDTH} from "../styles";
import MBTCommonLoader from "../components/common/MBTCommonLoader";
import KeepAwake from "react-native-keep-awake";

class RateServicesSeparatelyPage extends Component {

  state = {
    overAllRatingLevel: 0,
    loading: false
  };

  constructor() {
    super();
    this.onStarRatingPress = this.onStarRatingPress.bind(this)
  }

  componentWillMount() {
    KeepAwake.activate()
  }

  // componentWillUnmount() {
  //   KeepAwake.deactivate()
  // }

  componentDidMount() {
    this._initializeInactivityChecker();
  }

  _initializeInactivityChecker() {
    InactivityIntervalHandler.callback = this._initializeInactivityChecker.bind(this);
    InactivityIntervalHandler.data = this.props;
    InactivityIntervalHandler.start()
  }

  onStarRatingPress(serv, rating) {
    serv.ratingLevel = rating;
    this.forceUpdate();

    if (this.props.data.servicesUsed.filter(e => !e.ratingLevel).length === 0) {
      this.setState({
        loading: true
      })
      this.props.servicesSelected(this.props.data.servicesUsed).then(() => {
        setTimeout(() => {
          this.setState({
            loading: false
          })
          this.props.nav.navigateToNextPage()
        }, 350)
      })
    }
  }

  render() {
    return (
      <ScrollView contentContainerStyle={appStyles.container} keyboardDismissMode={'on-drag'}>
        <View style={appStyles.container}>
          <MBTCommonLoader loading={this.props.loading === true || this.state.loading === true}/>
          <MBTCommonNoInternetPage deviceIsOffline={false}/>
          <View style={appStyles.container}>
            <View style={{width: DEVICE_WIDTH, alignItems: 'center', justifyContent: 'center', marginTop: 20}}>
              <Text style={appStyles.title}>{'Would you please rate the Services'.toUpperCase()}</Text>
            </View>
            {this.props.data.servicesUsed.map(serv => {
              return (
                <View key={serv._id}
                      style={{marginTop: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                  <View style={appStyles.serviceItem}>
                    <Image source={{
                      uri: (serv.image === undefined ? Config.PLACEHOLDER_EMP_IMAGE : (Config.IMAGE_URL_PREFIX + serv.image)),
                      width: 80,
                      height: 80
                    }}/>
                    <Text>
                      {serv.name}
                    </Text>
                  </View>
                  <StarRating
                    disabled={false}
                    maxStars={5}
                    rating={serv.ratingLevel}
                    selectedStar={(rating) => this.onStarRatingPress(serv, rating)}
                    starSize={60}
                    emptyStar={require('../assets/star.png')}
                    fullStar={require('../assets/selected_star.png')}
                    buttonStyle={appStyles.starButtonStyle}
                  />
                </View>
              )
            })}
          </View>
        </View>
      </ScrollView>
    );
  }
}

const appStyles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1
  },
  starButtonStyle: {
    margin: 20
  },
  serviceItem: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    width: 150,
    margin: 15
  },
  title: {
    fontSize: 25,
    fontWeight: '700',
    color: '#303646'
  }
});

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    data: state.data,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline,
    nav: state.nav
  }
}

function mapDispatchToProps(dispatch) {
  return {
    inactivityReset: (data) => inactivityResetAction(dispatch, data),
    servicesSelected: (data) => servicesSelectedAction(dispatch, data)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RateServicesSeparatelyPage);