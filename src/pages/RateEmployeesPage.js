import React, {Component} from 'react';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {connect} from "react-redux";
import MBTCommonNoInternetPage from "../components/common/MBTCommonNoInternetPage";
import {InactivityIntervalHandler} from "../helpers/inactivity-reset.helper";
import {Config} from "../config";
import {inactivityResetAction} from "../redux/actions/inactivity-reset.actions";
import {employeesSelectedAction} from "../redux/actions/employees-selected.actions";
import MBTCommonLoader from "../components/common/MBTCommonLoader";
import KeepAwake from "react-native-keep-awake";
import chunkArray from "../helpers/array-chunk.helper";

import Toast from 'react-native-simple-toast';

class RateEmployeesPage extends Component {

  state = {
    overAllRatingLevel: 0,
    selectedEmployees: [],
    loading: false
  };

  constructor() {
    super();
    this.getStyle = this.getStyle.bind(this);
    this._onNext = this._onNext.bind(this);
  }

  componentWillMount() {
    KeepAwake.activate()
  }

  // componentWillUnmount() {
  //   KeepAwake.deactivate()
  // }

  componentDidMount() {
    this._initializeInactivityChecker();
  }

  _initializeInactivityChecker() {
    InactivityIntervalHandler.callback = this._initializeInactivityChecker.bind(this);
    InactivityIntervalHandler.data = this.props;
    InactivityIntervalHandler.start()
  }

  selectEmployee(employee) {
    InactivityIntervalHandler.userActivity();
    let selectedEmployees = this.state.selectedEmployees;
    let currentIndex = selectedEmployees.indexOf(employee);
    if (currentIndex >= 0) {
      selectedEmployees.splice(currentIndex, 1)
    } else if (selectedEmployees.length < 4) {
      selectedEmployees.push(employee)
    } else if (selectedEmployees.length === 4) {
      Toast.showWithGravity(
        `You can select only 4 employees`,
        Toast.LONG,
        Toast.CENTER
      );
    }

    this.setState({
      selectedEmployees
    })
  }

  _onNext() {
    this.setState({
      loading: true
    })
    this.props.employeesSelected(this.state.selectedEmployees).then(() => {
      setTimeout(() => {
        this.setState({
          loading: false
        })
        this.props.nav.navigateToNextPage()
      }, 350)
    })
  }

  getStyle(emp) {
    if (this.state.selectedEmployees.indexOf(emp) >= 0) {
      return appStyles.selectedEmployee
    }
    return appStyles.unSelectedEmployee
  }

  render() {

    let noEmplyees = this.props.user.employees.length;
    let cols = 4;
    let empimgprop = {
      width: 140, 
      height: 140, 
      contWidth: 240,
      contHeight: 210
    };
    if(noEmplyees <= 4){
      cols = 2;
      empimgprop.width = 220;
      empimgprop.height = 220;
      empimgprop.contWidth = 320;
      empimgprop.contHeight = 280;
    } else if(noEmplyees <= 6){
      cols = 3;
      empimgprop.width = 200;
      empimgprop.height = 200;
      empimgprop.contWidth = 280;
      empimgprop.contHeight = 260;
    } else if(noEmplyees <= 9){
      cols = 3;
      empimgprop.width = 175;
      empimgprop.height = 175;
      empimgprop.contWidth = 280;
      empimgprop.contHeight = 220;
    }

    return (
      <View style={appStyles.container} keyboardDismissMode={'on-drag'}>
        <View style={appStyles.container}>
          <MBTCommonLoader loading={this.props.loading === true || this.state.loading === true}/>
          <MBTCommonNoInternetPage deviceIsOffline={false}/>
          <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }} >
            <Text style={appStyles.title}>{this.props.user.employeePageHeader}</Text>

            <ScrollView>
            {chunkArray(this.props.user.employees, cols).map((arr, index) => {
              return (
                
                <View style={appStyles.employeesRow} key={index}>
                  {arr.map(emp => {
                    console.log(Config.IMAGE_URL_PREFIX + emp.image);
                    return (
                      <TouchableOpacity key={emp._id} onPress={this.selectEmployee.bind(this, emp)}
                          style={{
                            flexDirection: 'column',
                            alignItems: 'center',
                            height: empimgprop.contHeight,
                            width: empimgprop.contWidth,
                            margin: 0
                          }} activeOpacity={0.9}>
                        <Image style={this.getStyle(emp)} source={{
                          uri: (emp.image === undefined ? Config.PLACEHOLDER_EMP_IMAGE : (Config.IMAGE_URL_PREFIX + emp.image)),
                          width: empimgprop.width,
                          height: empimgprop.height
                        }}/>
                        <Text>
                          {emp.name}
                        </Text>
                        <Text>
                          {emp.position}
                        </Text>
                      </TouchableOpacity>
                    )
                  })}
                </View>
                
              )
            })}
            </ScrollView>
            <View style={{marginTop: 10, alignItems: 'center', marginBottom: 55}}>
              <TouchableOpacity
                style={appStyles.submitButton}
                onPress={this._onNext}
                activeOpacity={0.65}>
                <Text style={appStyles.submitButtonText}>NEXT</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const appStyles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center'
  },
  submitButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DA453A',
    width: 150,
    borderRadius: 30,
    padding: 8,
    zIndex: 100,
  },
  submitButtonText: {
    fontSize: 18,
    fontWeight: '100',
    color: '#fff',
  },
  employeesRow: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center', 
  },
  employeeItem: {
    flexDirection: 'column',
    alignItems: 'center',
    height: 210,
    width: 240,
    margin: 0
  },
  selectedEmployee: {
    borderColor: '#FA1C4A',
    borderWidth: 3,
    borderRadius: 4,
  },
  unSelectedEmployee: {
    borderRadius: 4,
  },
  title: {
    fontSize: 25,
    fontWeight: '700',
    color: '#303646', 
    textAlign: 'center', 
    marginTop: 25,
    marginBottom: 20,
  }
});

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    data: state.data,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline,
    nav: state.nav
  }
}

function mapDispatchToProps(dispatch) {
  return {
    inactivityReset: (data) => inactivityResetAction(dispatch, data),
    employeesSelected: (data) => employeesSelectedAction(dispatch, data)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RateEmployeesPage);