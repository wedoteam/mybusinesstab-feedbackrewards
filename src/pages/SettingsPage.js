import React, {Component} from 'react';
import {AsyncStorage, Picker, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import {loginAction} from "../redux/actions/login.actions";
import CheckBox from "react-native-check-box";
import Storage from 'react-native-storage';
import KeepAwake from "react-native-keep-awake";
import {DEVICE_WIDTH} from "../styles";
import DateTimePicker from 'react-native-modal-datetime-picker'
import moment from 'moment';
import chunkArray from "../helpers/array-chunk.helper";

class DayOnOff extends Component {

  state = {
    from: false,
    to: false,
  };

  constructor() {
    super();
    this._handleFromDatePicked = this._handleFromDatePicked.bind(this);
    this._handleToDatePicked = this._handleToDatePicked.bind(this);
  }

  togglePicker(type) {
    let obj = {};
    obj[type] = !this.state[type];
    this.setState(obj);
  }

  _handleFromDatePicked = (date) => {
    this.props.setData(this.props.day + 'From', moment(date).format('hh:mm A'));
    this.togglePicker('from');
  };

  _handleToDatePicked = (date) => {
    console.log(this.props.day + 'To', moment(date).format('hh:mm A'));
    this.props.setData(this.props.day + 'To', moment(date).format('hh:mm A'));
    this.togglePicker('to');
  };

  render() {
    const {
      day,
      data,
      toggle,
      setData
    } = this.props;

    console.log(this.state);

    const fromTime = data[day + 'From'];
    const toTime = data[day + 'To'];

    let fromTimeDate = new Date();
    let toTimeDate = new Date();

    if (fromTime !== '') {
      fromTimeDate = moment(fromTime, 'hh:mm A').toDate()
    }

    if (toTime !== '') {
      toTimeDate = moment(toTime, 'hh:mm A').toDate()
    }

    return (
      <View>
        <CheckBox
          style={{padding: 10, width: DEVICE_WIDTH / 3}}
          onClick={() => {
            toggle(day)
          }}
          isChecked={data[day]}
          rightText={day.toUpperCase()}
          rightTextStyle={{fontSize: 24, color: '#282e44'}}
        />

        {data[day] &&
        <View style={{width: DEVICE_WIDTH / 3, height: 50, flexDirection: 'row'}}>
          <View style={{width: DEVICE_WIDTH / 6, marginLeft: 46}}>
            <TouchableOpacity onPress={this.togglePicker.bind(this, 'from')}>
              <Text style={appStyles.inputLabel}>
                From
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.togglePicker.bind(this, 'from')} style={{flex: 1}}>
              <Text>{fromTime !== '' ? fromTime : 'Select a time'}</Text>
              <DateTimePicker
                isVisible={this.state.from}
                onConfirm={this._handleFromDatePicked}
                onCancel={this.togglePicker.bind(this, 'from')}
                is24Hour={false}
                datePickerModeAndroid={'spinner'}
                mode={'time'}
                date={fromTimeDate}
              />
            </TouchableOpacity>
          </View>

          <View style={{width: DEVICE_WIDTH / 6}}>
            <TouchableOpacity onPress={this.togglePicker.bind(this, 'to')}>
              <Text style={appStyles.inputLabel}>
                To
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.togglePicker.bind(this, 'to')} style={{flex: 1}}>
              <Text>{toTime !== '' ? toTime : 'Select a time'}</Text>
              <DateTimePicker
                isVisible={this.state.to}
                onConfirm={this._handleToDatePicked}
                onCancel={this.togglePicker.bind(this, 'to')}
                is24Hour={false}
                datePickerModeAndroid={'spinner'}
                mode={'time'}
                date={toTimeDate}
              />
            </TouchableOpacity>
          </View>
        </View>
        }
      </View>
    );
  }
}

class SettingsPage extends Component {

  state = {
    auto: false,

    lightPercentage: 40,
    dimPercentage: 0,

    monday: false,
    mondayFrom: '09:00 AM',
    mondayTo: '08:00 PM',

    tuesday: false,
    tuesdayFrom: '09:00 AM',
    tuesdayTo: '08:00 PM',

    wednesday: false,
    wednesdayFrom: '09:00 AM',
    wednesdayTo: '08:00 PM',

    thursday: false,
    thursdayFrom: '09:00 AM',
    thursdayTo: '08:00 PM',

    friday: false,
    fridayFrom: '09:00 AM',
    fridayTo: '08:00 PM',

    saturday: false,
    saturdayFrom: '09:00 AM',
    saturdayTo: '08:00 PM',

    sunday: false,
    sundayFrom: '09:00 AM',
    sundayTo: '08:00 PM',
  };

  componentWillMount() {
    KeepAwake.activate()
  }

  constructor() {
    super();
    this._saveToStorage = this._saveToStorage.bind(this);

    this.storage = new Storage({
      size: 1000,
      storageBackend: AsyncStorage,
      defaultExpires: null,
      enableCache: false
    });

    this.storage.load({
      key: 'settings',
    }).then(ret => {
      console.log("ret", ret);
      this.setState(ret);
    }).catch(err => {
      console.log("err", err)
    })
  }

  _saveToStorage() {
    return this.storage.save({
      key: 'settings',
      data: this.state
    }).then(() => {
      console.log("saved")
    }).catch(err => {
      console.log("err", err)
    })
  }

  toggle(type) {
    let obj = {};
    obj[type] = !this.state[type];
    obj[type + 'From'] = '09:00 AM';
    obj[type + 'To'] = '08:00 PM';
    this.setState(obj);

    setTimeout(this._saveToStorage, 50);
  }

  setData(key, value) {
    let obj = {};
    obj[key] = value;
    this.setState(obj);
    setTimeout(this._saveToStorage, 50);
  }

  render() {
    const percentageValues = []
    for (let i = 0; i <= 100; i++) {
      percentageValues.push(i)
    }
    return (
      <View style={appStyles.container}>
        <CheckBox
          style={{padding: 10, width: DEVICE_WIDTH / 2}}
          onClick={this.toggle.bind(this, 'auto')}
          isChecked={this.state.auto}
          rightText={'Schedule auto dim'}
          rightTextStyle={{fontSize: 24, color: '#282e44'}}
        />

        {this.state.auto &&
        <View>
          <View style={{flexDirection: 'row'}}>
            <Text>lighten tablet screen at </Text>
            <Picker
              selectedValue={this.state.lightPercentage ? this.state.lightPercentage : 100}
              mode={'dropdown'}
              style={{marginTop: -15, width: 120}}
              onValueChange={(itemValue) => {
                this.setState({
                  lightPercentage: itemValue
                })
                setTimeout(this._saveToStorage, 50);
              }}>
              {percentageValues.map((i, idx) => <Picker.Item key={idx + 'light'} label={`${i}%`} value={i}/>)}
            </Picker>
            <Text> on following days and dim to </Text>
            <Picker
              selectedValue={this.state.dimPercentage ? this.state.dimPercentage : 0}
              mode={'dropdown'}
              style={{marginTop: -15, width: 120}}
              onValueChange={(itemValue) => {
                this.setState({
                  dimPercentage: itemValue
                })
                setTimeout(this._saveToStorage, 50);
              }}>
              {percentageValues.map((i, idx) => <Picker.Item key={idx + 'dark'} label={`${i}%`} value={i}/>)}
            </Picker>
            <Text> any other time</Text>
          </View>

          {chunkArray(['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'], 2).map((row, index) => {
            return (
              <View style={{flexDirection: 'row'}} key={index + '-row'}>
                {row.map(day => {
                  return (
                    <DayOnOff
                      key={day}
                      toggle={this.toggle.bind(this)}
                      data={this.state}
                      setData={this.setData.bind(this)}
                      day={day}
                    />
                  )
                })}
              </View>
            )
          })}
        </View>
        }
      </View>
    );
  }
}

const appStyles = StyleSheet.create({
  container: {
    backgroundColor: '#EDEBEC',
    flex: 1,
    padding: 20,
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  inputLabel: {
    fontSize: 20,
    fontWeight: '100',
    color: '#282e44'
  },
});

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    error: state.app.error,
    loading: state.app.loading,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    loginUser: (loginDetails) => loginAction(dispatch, loginDetails)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SettingsPage)