import React, {Component} from 'react';
import {
  AsyncStorage,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  View, 
  Linking, 
  Modal, 
  KeyboardAvoidingView, 
  Image
} from 'react-native';
import {connect} from 'react-redux';
import {loginAction} from "../redux/actions/login.actions";
import MBTCommonLoader from "../components/common/MBTCommonLoader";
import MBTCommonNoInternetPage from "../components/common/MBTCommonNoInternetPage";
import CheckBox from "react-native-check-box";
import Storage from 'react-native-storage';
import {inactivityResetAction} from "../redux/actions/inactivity-reset.actions";
import {DEVICE_WIDTH, DEVICE_HEIGHT} from "../styles";
import KeepAwake from "react-native-keep-awake";
import MBTCommonSecretArea from "../components/common/MBTCommonSecretArea";
import {Actions} from 'react-native-router-flux'

import VersionCheck from 'react-native-version-check';

import Toast from 'react-native-simple-toast';

const logo = require('../assets/logo.png');

class LoginPage extends Component {

  state = {
    username: '',
    password: '',
    imageLoading: false,
    rememberMe: true
  };

  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      modalVisible: false,
      imageLoading: false,
      rememberMe: true
    };

    this._onSubmit = this._onSubmit.bind(this);
    this.demoTourLogin = this.demoTourLogin.bind(this);
    this.toggleCheckedState = this.toggleCheckedState.bind(this);

    this.storage = new Storage({
      size: 1000,
      storageBackend: AsyncStorage,
      defaultExpires: null,
      enableCache: false
    });

    this.storage.load({
      key: 'loginState',
    }).then(ret => {
      console.log("ret", ret);
      this.setState({
        username: ret.username,
        password: ret.password
      }); 

      this.props.loginUser({
      username: this.state.username,
      password: this.state.password
    }).then((loggedIn) => {
      if (!loggedIn) {
        return;
      }

      if (this.state.rememberMe) {
        this.storage.save({
          key: 'loginState',
          data: {
            username: this.state.username,
            password: this.state.password
          }
        }).then(() => {
          console.log("saved")
        }).catch(err => {
          console.log("err", err)
        })
      } else {
        this.storage.remove({
          key: 'loginState'
        })
      }

      this.setState({
        imageLoading: true
      });

      this.props.inactivityReset({
        fromLoginPage: true,

        userId: this.props.user._id,
        landingPageShow: this.props.user.feedBackPage.landingPage.show,
        employeeSectionAsFirstPage : this.props.user.employeeSectionAsFirstPage
      })
    })

    }).catch(err => {
      console.log("err", err)
    })
  }

  componentDidMount(){
    var currv = VersionCheck.getCurrentVersion();
    var storeURL = VersionCheck.getStoreUrl();
    VersionCheck.getLatestVersion()
    .then(async latestVersion => {
      var curVArray = currv.split(".");
      var latVArray = latestVersion.split(".");
      
      if(latVArray[1] > curVArray[1]){
        // alert('NO 2 UPDATE');
        this.setState({modalVisible: true}); 
      }
      if(latVArray[0] > curVArray[0]){
        // alert('NO 1 UPDATE');
        this.setState({modalVisible: true});
      }
    });
  }

  componentWillMount() {
    console.log("\n\nKEEPING AWAKE\n======================\n\n")
    KeepAwake.activate()
  }

  // componentWillUnmount() {
  //   KeepAwake.deactivate()
  // }

  _onSubmit() {
    this.props.loginUser({
      username: this.state.username,
      password: this.state.password
    }).then((loggedIn) => {
      if (!loggedIn) {
        return;
      }

      if (this.state.rememberMe) {
        this.storage.save({
          key: 'loginState',
          data: {
            username: this.state.username,
            password: this.state.password
          }
        }).then(() => {
          console.log("saved")
        }).catch(err => {
          console.log("err", err)
        })
      } else {
        this.storage.remove({
          key: 'loginState'
        })
      }

      this.setState({
        imageLoading: true
      });

      this.props.inactivityReset({
        fromLoginPage: true,

        userId: this.props.user._id,
        landingPageShow: this.props.user.feedBackPage.landingPage.show,
        employeeSectionAsFirstPage : this.props.user.employeeSectionAsFirstPage
      })
    })
  }

  demoTourLogin(){
    this.props.loginUser({
      username: 'demo',
      password: 'demodemo562'
    }).then((loggedIn) => {
      if (!loggedIn) {
        return;
      }      
      this.storage.remove({
        key: 'loginState'
      })
      this.setState({
        imageLoading: true
      });
      this.props.inactivityReset({
        fromLoginPage: true,
        userId: this.props.user._id,
        landingPageShow: this.props.user.feedBackPage.landingPage.show,
        employeeSectionAsFirstPage : this.props.user.employeeSectionAsFirstPage, 
        demoTour: true
      })
    })
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.error !== undefined) {
      setTimeout(() => {
        // ToastAndroid.showWithGravity(
        //   `${nextProps.error}`,
        //   ToastAndroid.LONG,
        //   ToastAndroid.CENTER
        // );
        Toast.showWithGravity(
          `${nextProps.error}`,
          Toast.LONG,
          Toast.CENTER
        );
      }, 100)
    }

  }

  render() {
    return (
      <KeyboardAvoidingView style={appStyles.container} behavior="padding" >

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            
          }}
          >
          <View style={{flex: 1, alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.7)', flexDirection: 'row', justifyContent: 'center'}}>
            <View style={{flexDirection: 'column', alignItems: 'center', justifyContent:'center', alignItems:'center', width: 600, height: 200, padding: 20, backgroundColor: '#ffffff' }}>
              <View style={{flex: 1}}>
                <Text style={{fontSize: 24, textAlign: 'center'}}>There is a Major release for the App that you must Update in order to continue using.</Text>
              </View>

              <View style={{flex: 1}}>
              <TouchableOpacity
                style={appStyles.submitButton}
                onPress={ async () => {
                  Linking.openURL(await VersionCheck.getStoreUrl());
                }}
                activeOpacity={0.65}>
                <Text style={appStyles.submitButtonText}>UPDATE NOW</Text>
              </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        <View style={{position: 'absolute', right: -150, flexDirection: 'column', justifyContent:'space-between', width: DEVICE_WIDTH*0.55, alignItems:'center', height: DEVICE_HEIGHT*0.25}}>
          <TouchableWithoutFeedback onPress={() => {
            Actions.push('settingsPage')
          }}>
            <View>
              <Text style={{fontSize: 20, color: '#282e44', marginBottom: 10, textAlign: 'left'}}>
                Settings
              </Text>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => {
            Linking.openURL('https://mybusinesstab.com');
          }}>
            <View>
              <Text style={{fontSize: 20, color: '#282e44', marginBottom: 10, textAlign: 'left'}}>
                Register now
              </Text>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => {
            this.demoTourLogin();
          }}>
            <View>
              <Text style={{fontSize: 20, color: '#282e44', marginBottom: 10, textAlign: 'left'}}>
                Demo Tour
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
  
        <MBTCommonSecretArea onActivated={() => {
          alert('secret area activated')
        }}/>
        <MBTCommonNoInternetPage deviceIsOffline={this.props.deviceIsOffline}/>
        <MBTCommonLoader loading={this.props.loading || this.state.imageLoading}/>
        
        <View style={{position: 'absolute', left: 50}}>
          <Image source={logo} style={{width:125, height: 125}} />
        </View>
        <View style={appStyles.signInWrapper}>
          <View>
            <Text style={{fontSize: 28, color: '#282e44', textDecorationLine: 'underline', marginBottom: 30}}>
              SIGN IN
            </Text>
          </View>

          <View>
            <Text style={appStyles.inputLabel}>
              User name
            </Text>
            <TextInput
              onChangeText={(username) => this.setState({username})}
              value={this.state.username}
              style={appStyles.input}
              secureTextEntry={false}
              autoCorrect={false}
              autoCapitalize='none'
              returnKeyType='next'
              onSubmitEditing={() => {
                this.passwordField.focus();
              }}
              blurOnSubmit={false}
              keyboardType='email-address'
              underlineColorAndroid='transparent'/>
          </View>

          <View style={{marginTop: 10}}>
            <Text style={appStyles.inputLabel}>
              Password
            </Text>
            <TextInput
              onChangeText={(password) => this.setState({password})}
              value={this.state.password}
              ref={(input) => {
                this.passwordField = input;
              }}
              style={appStyles.input}
              secureTextEntry={true}
              autoCorrect={false}
              autoCapitalize='none'
              returnKeyType='done'
              underlineColorAndroid='transparent'/>
          </View>

          <View style={{marginTop: 20}}>
            <CheckBox
              style={{padding: 10, width: DEVICE_WIDTH / 2}}
              onClick={this.toggleCheckedState}
              isChecked={this.state.rememberMe}
              rightText={'Remember Me'}
              rightTextStyle={{fontSize: 24, color: '#282e44'}}
            />
          </View>

          <View style={{marginTop: 30, flexDirection: 'row'}}>
            <TouchableOpacity
              style={appStyles.submitButton}
              onPress={this._onSubmit}
              activeOpacity={0.65}>
              <Text style={appStyles.submitButtonText}>SIGN IN</Text>
            </TouchableOpacity>            
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }

  toggleCheckedState() {
    this.setState({
      rememberMe: !this.state.rememberMe
    })
  }
}

const appStyles = StyleSheet.create({
  input: {
    width: DEVICE_WIDTH / 2,
    backgroundColor: '#EDEBEC',
    borderRadius: 30,
    color: '#555',
    padding: 10,
    paddingStart: 20,
    borderColor: '#fdc0b9',
    borderWidth: 1
  },
  inputLabel: {
    fontSize: 20,
    fontWeight: '100',
    color: '#282e44'
  },
  container: {
    backgroundColor: '#EDEBEC',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  signInWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    padding: 25,
    borderRadius: 20
  },
  submitButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DA453A',
    width: 150,
    borderRadius: 30,
    padding: 8,
    zIndex: 100,
    marginLeft: 10,
    marginRight: 10,
  },
  submitButtonText: {
    fontSize: 18,
    fontWeight: '100',
    color: '#fff'
  },
});

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    error: state.app.error,
    loading: state.app.loading,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline
  }
}

function mapDispatchToProps(dispatch) {
  return {
    loginUser: (loginDetails) => loginAction(dispatch, loginDetails),
    inactivityReset: (data) => inactivityResetAction(dispatch, data)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginPage)