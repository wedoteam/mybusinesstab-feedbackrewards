import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View, KeyboardAvoidingView} from 'react-native';
import {connect} from "react-redux";
import {submitCustomerDetailsAction} from "../redux/actions/customer-details.actions";
import {InactivityIntervalHandler} from "../helpers/inactivity-reset.helper";
import {inactivityResetAction} from "../redux/actions/inactivity-reset.actions";
import {DEVICE_WIDTH} from "../styles";
import MBTCommonNoInternetPage from "../components/common/MBTCommonNoInternetPage";
import MBTCommonLoader from "../components/common/MBTCommonLoader";
import KeepAwake from "react-native-keep-awake";

import Toast from 'react-native-simple-toast';

class CustomerDetailsPage extends Component {

  state = {
    customerName: '',
    customerExtra: '',
    loading: false
  };

  constructor() {
    super();
    this._onSubmit = this._onSubmit.bind(this)
  }

  // componentWillUnmount() {
  //   KeepAwake.deactivate()
  // }

  componentWillMount() {
    KeepAwake.activate()
    let data = {
      customerName: this.props.data.customerName
    };
    if (this.props.user.feedBackPage.customerDetailsExtraField.type === 'Email') {
      data.customerExtra = this.props.data.customerEmailId
    }

    this.setState(data)
  }

  componentDidMount() {
    InactivityIntervalHandler.callback = undefined;
    InactivityIntervalHandler.data = this.props;
    InactivityIntervalHandler.start()
  }

  _onSubmit() {
    InactivityIntervalHandler.userActivity()
    if (this.state.customerName.trim() === '') {
      this.customerDetailsMainField.focus();
      Toast.showWithGravity(
        `${this.props.user.feedBackPage.defaultPatientNameTitle} is required`,
        Toast.LONG,
        Toast.CENTER
      );
      return;
    }

    if (!this.props.user.feedBackPage.customerDetailsExtraField.optional && !this.state.customerExtra || this.state.customerExtra.trim() === '') {
      this.customerDetailsExtraField.focus();
      Toast.showWithGravity(
        `Input is required or not valid`,
        Toast.LONG,
        Toast.CENTER
      );
      return;
    }

    let email = ''
    if (this.props.user.feedBackPage.customerDetailsExtraField.type === 'Email') {
      email = this.state.customerExtra
    }

    if (email !== '') {
      let regExp = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
      if (!regExp.test(email)) {
        Toast.showWithGravity(
          `Invalid Email`,
          Toast.LONG,
          Toast.CENTER
        );

        return
      }
    }

    InactivityIntervalHandler.clearInterval()

    this.setState({
      loading: true
    })

    this.props.submitCustomerDetails({
      name: this.state.customerName,
      other: this.state.customerExtra
    }).then(() => {
      setTimeout(() => {
        this.setState({
          loading: false
        })
        this.props.nav.navigateToNextPage()
      }, 350)
    })
  }

  render() {


    return (
      <KeyboardAvoidingView style={appStyles.container} behavior="padding" >
        <MBTCommonLoader loading={this.props.loading === true || this.state.loading === true}/>
        <MBTCommonNoInternetPage deviceIsOffline={this.props.deviceIsOffline}/>
        
        {this.props.user.feedBackPage.customerDetailsInstructions.show && 
          <View style={{alignItems: 'center', justifyContent: 'center', marginBottom: 20}}>
            <Text style={{textAlign: 'center', fontSize: 30, fontWeight: '900', color: '#f99f50',marginBottom: 0, paddingRight: 80, paddingLeft: 80}}>{this.props.user.feedBackPage.customerDetailsInstructions.text}</Text>
          </View>
        }

        <View style={appStyles.signInWrapper}>
          <View>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={appStyles.inputLabel}>
                {this.props.user.feedBackPage.defaultPatientNameTitle}
              </Text>
            </View>
            <TextInput
              ref={(input) => {
                this.customerDetailsMainField = input;
              }}
              onChangeText={(customerName) => {
                InactivityIntervalHandler.userActivity();
                this.setState({customerName})
              }}
              value={this.state.customerName}
              style={appStyles.input}
              secureTextEntry={false}
              autoCorrect={false}
              autoCapitalize='none'
              autoFocus={this.state.customerName === ''}
              returnKeyType='next'
              onSubmitEditing={() => {
                this.customerDetailsExtraField.focus();
              }}
              blurOnSubmit={false}
              underlineColorAndroid='transparent'/>
          </View>

          {this.props.user.feedBackPage.showCustomerDetailsExtraField &&
          <View style={{marginTop: 10}}>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={appStyles.inputLabel}>
                {this.props.user.feedBackPage.customerDetailsExtraField.header}
              </Text>
            </View>
            <TextInput
              ref={(input) => {
                this.customerDetailsExtraField = input;
              }}
              onChangeText={(customerExtra) => {
                InactivityIntervalHandler.userActivity();
                this.setState({customerExtra})
              }}
              value={this.state.customerExtra}
              style={appStyles.input}
              secureTextEntry={false}
              autoCorrect={false}
              autoCapitalize='none'
              returnKeyType='done'
              keyboardType={this.props.user.feedBackPage.customerDetailsExtraField.type === 'Email' ? 'email-address' : (this.props.user.feedBackPage.customerDetailsExtraField.type === 'Number' ? 'decimal-pad' : 'default')}
              underlineColorAndroid='transparent'/>
          </View>
          }

          <View style={{marginTop: 30}}>
            <TouchableOpacity
              style={appStyles.submitButton}
              onPress={this._onSubmit}
              activeOpacity={0.65}>
              <Text style={appStyles.submitButtonText}>NEXT</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const appStyles = StyleSheet.create({
  input: {
    width: DEVICE_WIDTH - 500,
    backgroundColor: '#EDEBEC',
    borderRadius: 30,
    color: '#555',
    padding: 10,
    paddingStart: 20,
    borderColor: '#fdc0b9',
    borderWidth: 1, 
    fontSize: 20
  },
  inputLabel: {
    fontSize: 20,
    fontWeight: '500',
    color: '#282e44'
  },
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  signInWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    padding: 10,
    borderRadius: 20
  },
  submitButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DA453A',
    width: 150,
    borderRadius: 30,
    padding: 8,
    zIndex: 100,
  },
  submitButtonText: {
    fontSize: 18,
    fontWeight: '100',
    color: '#fff'
  },
});

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    data: state.data,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline,
    nav: state.nav
  }
}

function mapDispatchToProps(dispatch) {
  return {
    submitCustomerDetails: (customerDetails) => submitCustomerDetailsAction(dispatch, customerDetails),
    inactivityReset: (data) => inactivityResetAction(dispatch, data)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerDetailsPage)
