import React, {Component} from 'react';
import {ScrollView, StatusBar, StyleSheet, TouchableOpacity, WebView, Text, Image, View} from 'react-native';
import MBTCommonLoader from "../components/common/MBTCommonLoader";
import {connect} from "react-redux";
import {inactivityResetAction} from "../redux/actions/inactivity-reset.actions";
import {DEVICE_HEIGHT, DEVICE_WIDTH} from "../styles";
import MBTCommonNoInternetPage from "../components/common/MBTCommonNoInternetPage";
import {submitFeedbackAction} from "../redux/actions/submit-feedback.actions";
import KeepAwake from "react-native-keep-awake";

const progressimages = {
  p0: require('../images/progress/0.png'),
  p10: require('../images/progress/10.png'),
  p20: require('../images/progress/20.png'),
  p30: require('../images/progress/30.png'),
  p40: require('../images/progress/40.png'),
  p50: require('../images/progress/50.png'),
  p60: require('../images/progress/60.png'),
  p70: require('../images/progress/70.png'),
  p80: require('../images/progress/80.png'),
  p90: require('../images/progress/90.png'),
  p100: require('../images/progress/100.png'),
}

class ThankYouPage extends Component {

  state = {
    loading: false,
  };

  constructor() {
    super()
    this._onPagePress = this._onPagePress.bind(this)
    this._resetData = this._resetData.bind(this)
  }

  componentWillMount() {
    KeepAwake.activate()
  }

  // componentWillUnmount() {
  //   KeepAwake.deactivate()
  // }

  _onPagePress() {
    clearTimeout(this._interval)
    this._resetData()
  }

  _resetData() {
    this.setState({
      loading: true
    });
    this.props.inactivityReset({
      userId: this.props.user._id,
      landingPageShow: this.props.user.feedBackPage.landingPage.show,
      employeeSectionAsFirstPage : this.props.user.employeeSectionAsFirstPage
    })
  }

  componentDidMount() {
    this.setState({
      loading: true
    });
    this._interval = undefined;

    var email = this.props.user.feedBackPage.customerDetailsExtraField.type === 'Email' ? this.props.data.customerDetailsExtraFieldValue : this.props.data.customerEmailId;

    this.props.submitFeedback({
      ...this.props.data,
      customerEmailId: this.props.user.feedBackPage.customerDetailsExtraField.type === 'Email' ? this.props.data.customerDetailsExtraFieldValue : this.props.data.customerEmailId,
      feedbackRecordedOn: new Date().getTime(),
      createdOn: new Date().getTime(),
    }, this.props.user.sessionId).then(() => {
      
      this.setState({
        loading: false
      });
      this._interval = setTimeout(this._resetData, 15000)
    }); 
  }

  render() {
    let customerName = this.props.data.customerName ? this.props.data.customerName.toUpperCase() : '';

    let splittedName = customerName.split(' ')
    let firstname = splittedName.length > 0 ? splittedName[0] : '';
    let notes = this.props.data.notes ? this.props.data.notes : '';

    let targetPoints = this.props.data.targetPoints ? this.props.data.targetPoints : 0;
    let totalPoints = this.props.data.userPoints ? this.props.data.userPoints : 0;
    let lastUpdatedPoints = this.props.data.lastUpdatedPoints ? this.props.data.lastUpdatedPoints : 0;
    
    let progress = 0;
    let progressPerc = 0;
    if(targetPoints > 0){
      progress = (100 * totalPoints ) / targetPoints;
      let progressTmp = progress / 10;
      progressPerc = Math.round(progressTmp) * 10;
    }
    if(progressPerc > 0 && progressPerc <= 5){
      progressPerc = 10;
    }
    if(progressPerc > 95 && progressPerc < 100){
      progressPerc = 90;
    }
    
    // let totalPoints = this.state.userPoints ? this.state.userPoints : 0;
    // let lastUpdatedPoints = this.state.lastUpdatedPoints ? this.state.lastUpdatedPoints : 0; 
    // alert("Points : " + totalPoints + " : UPD :" + lastUpdatedPoints);

    let html = this.props.user.thankYouPage.unSatisfiedCustomer_subMessage;
    if (this.props.data.satisfiedCustomer) {
      html = this.props.user.thankYouPage.subMessage
    }

    let showProgress = html.includes('[graphs]');
    
    if(this.state.loading == false){
      html = html
      .replace('\[name\]', customerName)
      .replace('\[totalpoints\]', totalPoints)
      .replace('\[earnedpoints\]', lastUpdatedPoints)
      .replace('\[firstname\]', firstname)
      .replace('\[notes\]', notes)
      .replace('\[graphs\]', '')
    }

    let webViewHeight = DEVICE_HEIGHT;
    if(showProgress){
      webViewHeight = DEVICE_HEIGHT - 200;
    }

    return (
      <ScrollView contentContainerStyle={appStyles.container} keyboardDismissMode={'on-drag'}>
        <TouchableOpacity style={appStyles.container} activeOpacity={1} onPress={this._onPagePress}>
          <MBTCommonNoInternetPage deviceIsOffline={this.props.deviceIsOffline}/>
          <MBTCommonLoader loading={this.state.loading}/>
          
          {!this.state.loading && showProgress  &&
            <View style={appStyles.progressWrap}>
              <View style={appStyles.percentageWrap}>
                <Text style={appStyles.progressText}>{totalPoints}/{targetPoints}</Text>
              </View>
              {progressPerc == 0 && <Image source={progressimages.p0} />}
              {progressPerc == 10 && <Image source={progressimages.p10} />}
              {progressPerc == 20 && <Image source={progressimages.p20} />}
              {progressPerc == 30 && <Image source={progressimages.p30} />}
              {progressPerc == 40 && <Image source={progressimages.p40} />}
              {progressPerc == 50 && <Image source={progressimages.p50} />}
              {progressPerc == 60 && <Image source={progressimages.p60} />}
              {progressPerc == 70 && <Image source={progressimages.p70} />}
              {progressPerc == 80 && <Image source={progressimages.p80} />}
              {progressPerc == 90 && <Image source={progressimages.p90} />}
              {progressPerc == 100 && <Image source={progressimages.p100} />}
            </View>
          }
          {!this.state.loading &&
          <WebView
            scalesPageToFit={true}
            source={{html: html}}
            scrollEnabled={false}
            style={{
              width: DEVICE_WIDTH,
              height: webViewHeight
            }}/>}
            
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

const appStyles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }, 
  progressWrap:{
    marginTop: 25,
    marginBottom: 25,
  },
  percentageWrap: {
    position: 'absolute',
    top: 60,
    alignItems: 'center',
    justifyContent: 'center', 
    width: 150,
  }, 
  progressText: {
    textAlign: 'center',
    fontSize: 24,
    alignItems: 'center',
    justifyContent: 'center'
  }
});


function mapStateToProps(state) {
  return {
    user: state.auth.user,
    data: state.data,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    inactivityReset: (data) => inactivityResetAction(dispatch, data),
    submitFeedback: (collectedData, authValue) => submitFeedbackAction(dispatch, collectedData, authValue),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ThankYouPage)


