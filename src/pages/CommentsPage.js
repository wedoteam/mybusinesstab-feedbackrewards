import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, TextInput, Button, TouchableOpacity, Keyboard,TouchableHighlight, KeyboardAvoidingView, TouchableWithoutFeedback, View} from 'react-native';
import {connect} from "react-redux";
import MBTCommonNoInternetPage from "../components/common/MBTCommonNoInternetPage";
import {InactivityIntervalHandler} from "../helpers/inactivity-reset.helper";
import {inactivityResetAction} from "../redux/actions/inactivity-reset.actions";
import {DEVICE_WIDTH} from "../styles";
import {addUserCommentAction} from "../redux/actions/user-comments.actions";
import MBTCommonLoader from "../components/common/MBTCommonLoader";
import KeepAwake from "react-native-keep-awake";

class CommentsPage extends Component {

  state = {
    comments: '',
    loading: false,
  };

  constructor() {
    super();
    this.state = {
      doneButton: false
    }
    this._onSubmit = this._onSubmit.bind(this);
    this._onWriteComment = this._onWriteComment.bind(this);
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  
  componentWillMount() {
    KeepAwake.activate();
  }

  componentWillUnmount() {
    // KeepAwake.deactivate()
    // this.keyboardDidShowListener.remove();
  }
  _keyboardDidShow () {
    this.setState({
      doneButton: true
    })
  }

  _keyboardDidHide () {
    this.setState({
      doneButton: false
    });
    Keyboard.dismiss;
  }

  componentDidMount() {
    InactivityIntervalHandler.callback = undefined;
    InactivityIntervalHandler.data = this.props;
    InactivityIntervalHandler.start()
  }

  _onSubmit() {
    InactivityIntervalHandler.userActivity();
    this.setState({
      loading: true
    })
    this.props.addUserComment(this.state.comments).then(() => {
      setTimeout(() => {
        this.setState({
          loading: false
        })
        this.props.nav.navigateToNextPage()
      }, 350)
    })
  }

  _onWriteComment(text) {
    InactivityIntervalHandler.userActivity();
    this.setState({
      comments: text
    })
  }


  render() {
    return (
      <View style={appStyles.container} keyboardDismissMode={'on-drag'}>
        <KeyboardAvoidingView behavior='padding' style={appStyles.container}>
          <MBTCommonLoader loading={this.props.loading === true || this.state.loading === true}/>
          <MBTCommonNoInternetPage deviceIsOffline={false}/>
          <View style={appStyles.container}>
            <Text style={appStyles.title}>{this.props.user.comment.title.toUpperCase()}</Text>
            <View style={appStyles.titleUnderline}/>
            <View style={{flexDirection:"row"}}>
              <TextInput
                multiline={true}
                underlineColorAndroid={'transparent'}
                numberOfLines={8}
                returnKeyType={'send'}
                style={appStyles.input}
                onChangeText={(text) => this._onWriteComment(text)}
                value={this.state.comments}/>
            </View>
              <TouchableOpacity
                style={appStyles.submitButton}
                onPress={this._onSubmit}
                activeOpacity={0.65}>
                <Text style={appStyles.submitButtonText}>NEXT</Text>
              </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const appStyles = StyleSheet.create({

  absButton:{
    alignItems:'flex-end', 
    justifyContent: 'flex-end', 
    flexDirection: 'row', 
  },

  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  input: {
    width: DEVICE_WIDTH / 2,
    backgroundColor: '#EDEBEC',
    borderRadius: 20,
    color: '#555',
    padding: 20,
    paddingStart: 20,
    borderColor: '#fdc0b9',
    borderWidth: 1,
    marginTop: 30,
    textAlignVertical: 'top', 
    lineHeight: 16, height: 120, 
    fontSize: 18
  },
  title: {
    fontSize: 40,
    fontWeight: '900',
    color: '#303646',
  },
  titleUnderline: {
    backgroundColor: '#303646',
    width: 150,
    height: 1
  },
  submitButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DA453A',
    width: 150,
    borderRadius: 30,
    padding: 8,
    zIndex: 100,
    marginTop: 20
  },
  submitButtonText: {
    fontSize: 18,
    fontWeight: '100',
    color: '#fff'
  },
});

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    data: state.data,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline,
    nav: state.nav
  }
}

function mapDispatchToProps(dispatch) {
  return {
    inactivityReset: (data) => inactivityResetAction(dispatch, data),
    addUserComment: (data) => addUserCommentAction(dispatch, data),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CommentsPage);