import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {connect} from "react-redux";
import MBTCommonNoInternetPage from "../components/common/MBTCommonNoInternetPage";
import StarRating from "react-native-star-rating";
import {InactivityIntervalHandler} from "../helpers/inactivity-reset.helper";
import {inactivityResetAction} from "../redux/actions/inactivity-reset.actions";
import {DEVICE_WIDTH} from "../styles";
import {detailRatingAction} from "../redux/actions/detail-rating.actions";
import MBTCommonLoader from "../components/common/MBTCommonLoader";
import KeepAwake from "react-native-keep-awake";

class RateAttributesPage extends Component {

  state = {
    overAllRatingLevel: 0,
    loading: false
  };

  constructor() {
    super();
    this.onStarRatingPress = this.onStarRatingPress.bind(this)
  }

  componentWillMount() {
    KeepAwake.activate()
  }

  // componentWillUnmount() {
  //   KeepAwake.deactivate()
  // }

  componentDidMount() {
    this._initializeInactivityChecker();
  }

  _initializeInactivityChecker() {
    InactivityIntervalHandler.callback = this._initializeInactivityChecker.bind(this);
    InactivityIntervalHandler.data = this.props;
    InactivityIntervalHandler.start()
  }

  onStarRatingPress(rating, attr) {
    attr.ratingLevel = rating;
    this.forceUpdate();

    if ([1, 2, 3].map(idx => this.props.data.detailRating[`attribute${idx}`]).filter(e => !e.ratingLevel).length === 0) {
      this.setState({
        loading: true
      })
      this.props.detailRatingAction(this.props.data.detailRating).then(() => {
        setTimeout(() => {
          this.setState({
            loading: false
          })
          this.props.nav.navigateToNextPage()
        }, 350)
      })
    }
  }

  render() {
    return (
      <View style={appStyles.container} keyboardDismissMode={'on-drag'}>
        <View style={appStyles.container}>
          <MBTCommonLoader loading={this.props.loading === true || this.state.loading === true}/>
          <MBTCommonNoInternetPage deviceIsOffline={false}/>
          <View style={appStyles.container}>
            <Text style={appStyles.title}>{this.props.data.detailRating.headerQuestion}</Text>
            <View style={appStyles.titleUnderline}/>
            {[1, 2, 3].map(i => {
              return (
                <View key={i} style={{
                  marginTop: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: DEVICE_WIDTH,
                  paddingLeft: 50,
                  paddingRight: 50
                }}>
                  <Text style={appStyles.attributeStyle}>
                    {this.props.data.detailRating[`attribute${i}`].attribute}
                  </Text>
                  <StarRating
                    disabled={false}
                    maxStars={5}
                    rating={this.props.data.detailRating[`attribute${i}`].ratingLevel}
                    selectedStar={(rating) => this.onStarRatingPress(rating, this.props.data.detailRating[`attribute${i}`])}
                    starSize={60}
                    emptyStar={require('../assets/star.png')}
                    fullStar={require('../assets/selected_star.png')}
                    buttonStyle={appStyles.starButtonStyle}
                  />
                </View>
              )
            })}
          </View>
        </View>
      </View>
    );
  }
}

const appStyles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: 40,
    fontWeight: '900',
    color: '#303646',
  },
  titleUnderline: {
    backgroundColor: '#303646',
    width: 150,
    height: 1
  },
  starButtonStyle: {
    margin: 20
  },
  earnRewardsText: {
    fontSize: 30,
    fontWeight: '100',
    color: '#303646',
  },
  rewardText: {
    fontSize: 30,
    fontWeight: '100',
    color: '#FA1C4A',
  },
  attributeStyle: {
    fontSize: 32,
    fontWeight: '400',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginRight: 25,
    width: DEVICE_WIDTH / 5
  }
});

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    data: state.data,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline,
    nav: state.nav
  }
}

function mapDispatchToProps(dispatch) {
  return {
    inactivityReset: (data) => inactivityResetAction(dispatch, data),
    detailRatingAction: (data) => detailRatingAction(dispatch, data)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RateAttributesPage);