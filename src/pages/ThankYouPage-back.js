import React, {Component} from 'react';
import {ScrollView, StatusBar, StyleSheet, TouchableOpacity, WebView, Text} from 'react-native';
import MBTCommonLoader from "../components/common/MBTCommonLoader";
import {connect} from "react-redux";
import {inactivityResetAction} from "../redux/actions/inactivity-reset.actions";
import {DEVICE_HEIGHT, DEVICE_WIDTH} from "../styles";
import MBTCommonNoInternetPage from "../components/common/MBTCommonNoInternetPage";
import {submitFeedbackAction} from "../redux/actions/submit-feedback.actions";
import KeepAwake from "react-native-keep-awake";

class ThankYouPage extends Component {

  state = {
    loading: false,
    userPoints: 0,
    lastUpdatedPoints: 0
  };

  constructor() {
    super()
    this._onPagePress = this._onPagePress.bind(this)
    this._resetData = this._resetData.bind(this)
  }

  componentWillMount() {
    KeepAwake.activate()
  }

  // componentWillUnmount() {
  //   KeepAwake.deactivate()
  // }

  _onPagePress() {
    clearTimeout(this._interval)
    this._resetData()
  }

  _resetData() {
    this.setState({
      loading: true
    });
    this.props.inactivityReset({
      userId: this.props.user._id,
      landingPageShow: this.props.user.feedBackPage.landingPage.show,
    })
  }

  componentDidMount() {
    this.setState({
      loading: true
    });
    this._interval = undefined;

    var email = this.props.user.feedBackPage.customerDetailsExtraField.type === 'Email' ? this.props.data.customerDetailsExtraFieldValue : this.props.data.customerEmailId;

    this.props.submitFeedback({
      ...this.props.data,
      customerEmailId: this.props.user.feedBackPage.customerDetailsExtraField.type === 'Email' ? this.props.data.customerDetailsExtraFieldValue : this.props.data.customerEmailId,
      feedbackRecordedOn: new Date().getTime(),
      createdOn: new Date().getTime(),
    }, this.props.user.sessionId).then(() => {
      
      this.setState({
        userPoints: this.props.data.userPoints, 
        lastUpdatedPoints: this.props.data.lastUpdatedPoints
      });

      this.setState({
        loading: false
      });
      
      // this._interval = setTimeout(this._resetData, 15000)
    }); 
  }

  render() {
    let customerName = this.props.data.customerName ? this.props.data.customerName.toUpperCase() : '';
    let splittedName = customerName.split(' ')
    let firstname = splittedName.length > 0 ? splittedName[0] : '';
    let notes = this.props.data.notes ? this.props.data.notes : '';

    // let totalPoints = this.props.data.userPoints ? this.props.data.userPoints : 0;
    // let lastUpdatedPoints = this.props.data.lastUpdatedPoints ? this.props.data.lastUpdatedPoints : 0;
    
    let totalPoints = this.state.userPoints ? this.state.userPoints : 0;
    let lastUpdatedPoints = this.state.lastUpdatedPoints ? this.state.lastUpdatedPoints : 0; 
    
    let html = this.props.user.thankYouPage.unSatisfiedCustomer_subMessage;
    if (this.props.data.satisfiedCustomer) {
      html = this.props.user.thankYouPage.subMessage
    }

    html = html
      .replace('\[name\]', customerName)
      .replace('\[totalpoints\]', totalPoints)
      .replace('\[earnedpoints\]', lastUpdatedPoints)
      .replace('\[firstname\]', firstname)
      .replace('\[notes\]', notes)


    return (
      <ScrollView contentContainerStyle={appStyles.container} keyboardDismissMode={'on-drag'}>
        <TouchableOpacity style={appStyles.container} activeOpacity={1} onPress={this._onPagePress}>
          <MBTCommonNoInternetPage deviceIsOffline={this.props.deviceIsOffline}/>
          <MBTCommonLoader loading={this.state.loading}/>
          <Text>{JSON.stringify(this.state)}</Text>
          <WebView
            scalesPageToFit={true}
            source={{html: html}}
            style={{
              width: DEVICE_WIDTH * 0.859375,
              height: DEVICE_HEIGHT * 0.99,
              marginTop: (DEVICE_HEIGHT * 0.04) + StatusBar.currentHeight,
              marginBottom: ((DEVICE_HEIGHT) * 0.04)
            }}/>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

const appStyles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});


function mapStateToProps(state) {
  return {
    user: state.auth.user,
    data: state.data,
    settings: state.app.settings,
    deviceIsOffline: state.app.deviceIsOffline,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    inactivityReset: (data) => inactivityResetAction(dispatch, data),
    submitFeedback: (collectedData, authValue) => submitFeedbackAction(dispatch, collectedData, authValue),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ThankYouPage)


