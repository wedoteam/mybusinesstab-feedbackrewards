import {Config} from '../config';

export default userFeedbackService = (requestBody, authValue) => {
  const headers = {
    'Content-Type': 'application/json;charset=UTF-8',
    'Authorization': `bearer ${authValue}`,
    'apikey': Config.API_KEY
  };
  
  console.log(JSON.stringify(requestBody)); 
  return fetch(`${Config.API_URL}/customer/customerFeedback?date=${new Date().getTime()}`, {
    cache: "no-store",
    headers: headers,
    body: JSON.stringify(requestBody),
    method: 'POST'
  }).then(response => {
    return response.json().then(data => {
      // alert(JSON.stringify(data)); 
      return Promise.resolve(data);
    })
  }).catch(err => {
    return Promise.reject(err)
  })
};