import {Config} from '../config';

export default checkUserService = (data, authValue) => {
  const headers = {
    'Content-Type': 'application/json;charset=UTF-8',
    'Authorization': `bearer ${authValue}`,
    'apikey': Config.API_KEY
  };

  return fetch(`${Config.API_URL}/customer/secure/checkUser?date=${new Date().getTime()}`, {
    cache: "no-store",
    headers: headers,
    body: JSON.stringify(data),
    method: 'POST'
  }).then(response => {
    // alert(response);
    return response.json().then(newData => {
      console.log(`/customer/secure/checkUser`, data, newData);
      return Promise.resolve(newData)
    })
  })
};