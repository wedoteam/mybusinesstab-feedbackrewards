import {Config} from '../config';

export default userLoginService = (data) => {
  const headers = {
    'Content-Type': 'application/json;charset=UTF-8',
    'apikey': Config.API_KEY
  };

  console.log("REQUEST: /signin", JSON.stringify(data));

  return fetch(`${Config.API_URL}/signin?date=${new Date().getTime()}`, {
    cache: "no-store",
    headers: headers,
    body: JSON.stringify(data),
    method: 'POST'
  }).then(response => {

    // alert(JSON.stringify(response));

    return response.json().then(newData => {
      // console.log("SUCCESS: /signin", JSON.stringify(newData));
      return Promise.resolve(newData)
    })
  })
};