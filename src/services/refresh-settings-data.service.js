import {Config} from '../config';

export default refreshSettingsDataService = (userId) => {
  const headers = {
    'apikey': Config.API_KEY
  };

  return fetch(`${Config.API_URL}/getCustomer/${userId}?date=${new Date().getTime()}`, {
    cache: "no-store",
    headers: headers,
    method: 'GET'
  }).then(response => {

    // alert(JSON.stringify(response));

    return response.json().then(data => {
      console.log(`/getCustomer/${userId}`, data);
      return Promise.resolve(data)
    })
  })
};