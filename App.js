import React, {Component} from 'react';
import {Router, Scene, Stack} from "react-native-router-flux";
import {Provider} from "react-redux";
import store from "./src/redux/reducers";
import {ConnectionInfo} from "./src/helpers/connectino-info.helper";

import LoginPage from './src/pages/LoginPage';
import LandingPage from "./src/pages/LandingPage";
import OverAllRatingPage from "./src/pages/OverAllRatingPage";
import RateAttributesPage from "./src/pages/RateAttributesPage";
import RateEmployeesPage from "./src/pages/RateEmployeesPage";
import RateEmployeesSeparatelyPage from "./src/pages/RateEmployeesSeparatelyPage";
import RateServicesPage from "./src/pages/RateServicesPage";
import RateServicesSeparatelyPage from "./src/pages/RateServicesSeparatelyPage";
import PhoneNumberPage from "./src/pages/PhoneNumberPage";
import CustomerDetailsPage from "./src/pages/CustomerDetailsPage";
import RewardsPage from "./src/pages/RewardsPage";
import CommentsPage from "./src/pages/CommentsPage";
import ThankYouPage from "./src/pages/ThankYouPage";
import SettingsPage from "./src/pages/SettingsPage";
import Storage from "react-native-storage";
import {AsyncStorage, Platform} from "react-native";
import moment from 'moment';
import * as DeviceBrightness from "react-native-device-brightness";

import { StatusBar } from 'react-native';

import { Immersive } from 'react-native-immersive'

export default class MBTFeedbackApp extends Component {

  constructor() {
    super(); 
    this._brightnessControl = this._brightnessControl.bind(this)
  }

  componentDidMount() {
    // if(Platform.os != 'ios'){
    //   Immersive.on();
    // }
    StatusBar.setHidden(true);
    ConnectionInfo.startWatch()

    this.storage = new Storage({
      size: 1000,
      storageBackend: AsyncStorage,
      defaultExpires: null,
      enableCache: false
    });

    this.intervalInt = setInterval(() => {
      this._brightnessControl();
    }, 500 * 60);
  }

  _brightnessControl() {
    this.storage.load({
      key: 'settings',
    }).then(ret => {
      if(ret.auto === true) {
        let time = moment();
        let day = moment().format('dddd').toLowerCase()
        if(ret[day] === true) {
          let fromTime = moment(ret[day + 'From'], 'hh:mm A');
          let toTime = moment(ret[day + 'To'], 'hh:mm A');
          if(time.isBetween(fromTime, toTime)) {
            DeviceBrightness.setBrightnessLevel(parseInt(ret.lightPercentage)/100);
          } else {
            DeviceBrightness.setBrightnessLevel(parseInt(ret.dimPercentage)/100);
          }
        } else {
          DeviceBrightness.getSystemBrightnessLevel()
          .then(function (luminous) {
            DeviceBrightness.setBrightnessLevel(luminous); 
          });
        }
      } else {
        DeviceBrightness.getSystemBrightnessLevel()
        .then(function (luminous) {
          DeviceBrightness.setBrightnessLevel(luminous); 
        });
      }
    }).catch(err => {
      console.log("err", err)
    })
  }

  componentWillUnmount() {
    ConnectionInfo.stopWatch()
    clearInterval(this.intervalInt)
  }

  render() {
    return (
      <Provider store={store}>
        <Router>
          <Stack key="root">
            <Scene key="login" component={LoginPage} hideNavBar={true}/>
            <Scene key="settingsPage" component={SettingsPage} hideNavBar={false}/>
            <Scene key="landingPage" component={LandingPage} hideNavBar={true}/>
            <Scene key="overAllRatingPage" component={OverAllRatingPage} hideNavBar={true}/>
            <Scene key="rateAttributesPage" component={RateAttributesPage} hideNavBar={true}/>
            <Scene key="rateEmployeesPage" component={RateEmployeesPage} hideNavBar={true}/>
            <Scene key="rateEmployeesSeparatelyPage" component={RateEmployeesSeparatelyPage} hideNavBar={true}/>
            <Scene key="rateServicesPage" component={RateServicesPage} hideNavBar={true}/>
            <Scene key="rateServicesSeparatelyPage" component={RateServicesSeparatelyPage} hideNavBar={true}/>
            <Scene key="phoneNumberPage" component={PhoneNumberPage} hideNavBar={true}/>
            <Scene key="customerDetailsPage" component={CustomerDetailsPage} hideNavBar={true}/>
            <Scene key="rewardsPage" component={RewardsPage} hideNavBar={true}/>
            <Scene key="commentsPage" component={CommentsPage} hideNavBar={true}/>
            <Scene key="thankYouPage" component={ThankYouPage} hideNavBar={true}/>
          </Stack>
        </Router>
      </Provider>
    );
  }
}